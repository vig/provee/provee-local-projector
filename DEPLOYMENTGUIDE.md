<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://user-images.githubusercontent.com/47860858/115148201-cd7fb200-a05e-11eb-8487-075346083792.png" alt="Project logo"></a>
</p>

<h3 align="center">PROVEE - PROgressiVe Explainable Embeddings</h3>

<div align="center">


[![Status](https://img.shields.io/badge/status-active-success.svg)]()

[![Gitlab pipeline status](https://git.science.uu.nl/vig/provee/provee-local-projector/badges/master/pipeline.svg)](https://git.science.uu.nl/vig/provee/provee-local-projector/-/pipelines)
[![Gitlab coverage](https://git.science.uu.nl/vig/provee/provee-local-projector/badges/master/coverage.svg)]()
[![Documentation Status](https://readthedocs.org/projects/provee-local-projector/badge/?version=latest)](https://provee-local-projector.readthedocs.io/en/latest/?badge=latest)

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> 

Deep Neural Networks (DNNs), and their resulting **latent or embedding data spaces, are key to analyzing big data** in various domains such as vision, speech recognition, and natural language processing (NLP). However, embedding spaces are high-dimensional and abstract, thus not directly understandable. We aim to develop a software framework to visually explore and explain how embeddings relate to the actual data fed to the DNN. This enables both DNN developers and end-users to understand the currently black-box working of DNNs, leading to better-engineered networks, and explainable, transparent DNN systems whose behavior can be trusted by their end-users. 

Our central aim is to open DNN black-boxes, making complex data understandable for data science novices, and raising trust/transparency are core topics in VA and NLP research. PROVEE will advertise and apply VA in a wider scope with impact across sciences (medicine, engineering, biology, physics) where researchers use big data and deep learning.
</p>

## 📝 Table of Contents

- [Deployment Guide](#guide)
- [Hardware](#hardware)
- [Software](#software)

## 🧐 Deployment Guide <a name = "guide"></a>

empty for now


## 🧰 Hardware Requirements <a name = "hardware"></a>

empty for now


## 💾 Software Requirements <a name = "software"></a>

empty for now