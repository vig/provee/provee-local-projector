import os
import pickle as pk
import sys
import time
import threading
import pyqtgraph as pg
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QThread, QThreadPool, QTimer, QObject
from src.Renderers.Renderer2D import Renderer2D
from src.Renderers.Renderer3D import Renderer3D
from src.Renderers.VispyRenderer2D import VispyRenderer
from src.Renderers.DensityRenderer import DensityRenderer
from src.UI.ui import UI
from src.serviceManager import ServiceManager
from tabulate import tabulate
from src.constants import TRANSFORMING_BUFFER_SIZE
import numpy as np


class Provee(QObject):
    """Main object for the program, has acces to the UI, and service manager (which handles the connections to the microservices)

    Args:
        QObject (QObject): So that we can use QT signals/slots
    """

    def __init__(self, ui):
        super(Provee, self).__init__()
        self.ui = ui

        self.serviceManager = ServiceManager()
        self.models = []

        self.activeModel = None
        self.activeFilePath = None

        self.startVisualisation()
        self.handleConnections()
        self.ui.show()

        self.setupKNN()
        self.setupProjector()
        self.setupTransformer()

    def startVisualisation(self):
        """Creates the visualiser and passes it to the UI
        """
        self.visualiser = DensityRenderer(1)
        if isinstance(self.visualiser, VispyRenderer):
            self.visualiser.foundPointSignal.connect(
                self.clickedPointKNNRequest)
        self.ui.setVisualiser(self.visualiser.getWidget())

    def handleConnections(self):
        """Connects to the button clicked signals, comboBoxes selection changed etc.
        """
        self.ui.uploadFileBTN.clicked.connect(self.uploadFile)
        self.ui.projectionSwitch.clicked.connect(self.switchDensity)
        self.ui.getPrecisionPCA.clicked.connect(self.getPrecision)
        self.ui.getProjectionBTN.clicked.connect(self.getProjection)
        self.ui.knnRequestBTN.clicked.connect(self.clickedButtonKNNRequest)
        self.ui.stopProjectionBTN.clicked.connect(self.stopProjection)
        self.ui.removePointsBTN.clicked.connect(
            self.removePoints)
        self.ui.selectModel.currentIndexChanged.connect(self.modelChanged)
        self.ui.selectProjector.currentTextChanged.connect(
            self.projectorChanged)
        self.ui.loadModelBTN.clicked.connect(self.loadModel)
        self.ui.saveModelBTN.clicked.connect(self.saveModel)
        self.ui.btn_close.clicked.connect(self.closeWindow)
        self.ui.selectDataDirectoryBTN.clicked.connect(self.setDataDirectory)
        self.ui.downloadProjectionBTN.clicked.connect(
            self.downloadProjectionData)
        self.ui.uploadProjectionBTN.clicked.connect(self.uploadProjectionData)
        self.ui.findWordBTN.clicked.connect(self.findWordInVisualiser)
        self.ui.wordTable.clicked.connect(self.clickedWordInTable)
        self.ui.densityRequestBTN.clicked.connect(self.densityRequest)
        self.ui.densityRequestSwitch.clicked.connect(self.switchDensityRequest)
        self.ui.homeSwitchBTN.clicked.connect(self.switchToHomeRenderer)

    def densityRequest(self):
        wordRequest = self.ui.densityRequest.text()
        t = threading.Thread(
            target=self.serviceManager.makeDensityRequest, args=(wordRequest,))
        self.serviceManager.densityRequestSignal.connect(
            self.finishedDensityRequest)
        t.start()

    def finishedDensityRequest(self, indices, distances):
        self.visualiser.storeDensityRequestResults(indices, distances)

    def clickedWordInTable(self, item):
        self.visualiser.goToWordAnimated(
            self.ui.wordTable, item, int(self.ui.animatedSeconds.text()))

    def findWordInVisualiser(self):
        """Find all words with the given prefix.
        """

        word = self.ui.findPointField.text()
        if word == "":
            print("No word entered")
            return

        if not self.visualiser:
            print("No visualiser ready")
            return

        wordList, indices = self.visualiser.findWord(word)
        wordList.sort(key=len)
        self.ui.addWordsToTables(wordList)

    def switchToHomeRenderer(self):
        if isinstance(self.visualiser, DensityRenderer):
            self.visualiser.switchToNormal()

    def switchDensityRequest(self):
        if isinstance(self.visualiser, DensityRenderer):
            self.visualiser.switchToDensityRequest()

    def switchDensity(self):
        if isinstance(self.visualiser, DensityRenderer):
            self.visualiser.switchToDensity()

    def removePoints(self):
        self.visualiser.removeAllPoints()
        self.ui.clearWordTable()

    def getProjection(self):
        """Handler for the getprojection button
        """
        if not self.activeFilePath:
            print("No file active")
            return

        if not self.activeModel:
            print("No active model")
            return

        if not self.ui.addProjection.isChecked():
            self.removePoints()

        # Timers cant be started by the new thread, so do it here
        self.serviceManager.transformerPercentageTimer.start(1000)
        self.serviceManager.startTransformer(
            self.activeFilePath, self.activeModel)

    def uploadProjectionData(self):
        """Upload a projected data set
        """
        filePath = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, 'Select File', ".//SavedProjections")[0]

        if filePath == '':
            return

        if not self.ui.addProjection.isChecked():
            self.removePoints()

        self.visualiser.uploadProjectionData(filePath)

    def downloadProjectionData(self):
        """Stores the downloaded projection dataset in a file
        """

        # No visualiser yet
        if not self.visualiser:
            return

        filePath = ".//SavedProjections//dataset.txt"
        self.visualiser.downloadDataset(filePath)

    def modelChanged(self, value):
        """When a different model is selected in the model selection comboBox

        Args:
            value (int): The index of the selected model
        """
        # That one is empty
        if(value == 0):
            return
        selectedModel = self.models[value - 1]
        self.activeModel = selectedModel

    def saveModel(self):
        pk.dump(self.activeModel, open("SavedModels//SavedModel.pkl", "wb"))

    def loadModel(self):
        """Load in a model, note that this model must comply with the language used (python with python, cpp with cpp)
        """

        # Select a model
        filePath = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, 'Select File', ".//SavedModels")[0]

        if filePath == "":
            return

        with open(filePath, "rb") as f:
            model = f.read()

        self.models.append(model)

        fileName = filePath.split('/')[-1].split('.')[0]
        self.ui.addModelToList(fileName)

    def projectorChanged(self, name):
        """When a different projector is selected in the projector selection comboBox, then set the new projector.

        Args:
            name (string): The name of the selected projector
        """
        if (name == "" and not self.serviceManager.activeService['Projector']) or name == self.serviceManager.activeService['Projector']:
            return
        succesful = self.serviceManager.changeProjector(name)
        if not succesful:
            currentService = self.serviceManager.activeService['Projector']
            self.ui.selectProjector.setCurrentText(
                currentService if currentService else "")
        else:
            self.setFile(self.activeFilePath, enableKNN=False)

    # WIP

    def clickedPointKNNRequest(self, index):
        # Temp disabled
        return
        knnWord = self.idToWord[index]
        print("Clicked kNN", knnWord)
        self.getKNNRequest(knnWord)

    # WIP
    def clickedButtonKNNRequest(self):
        knnRequest = self.ui.knnRequest.text()
        self.getKNNRequest(knnRequest)

    def getKNNRequest(self, knnRequest):
        """Gets the request for a semantic KNN request, start up a thread which handles that
        """
        if not self.serviceManager.isServiceActive('KNN'):
            return

        # Since takes long time separate thread
        t = threading.Thread(
            target=self.serviceManager.getKNNRequest, args=(knnRequest,))
        self.serviceManager.knnRequestSignal.connect(self.finishedKnnRequest)
        t.start()

    # WIP
    def finishedKnnRequest(self, distanceList, wordList, wordIndexList):
        tableForm = tabulate(zip(wordList, distanceList),
                             headers=['Word', 'Distance'])
        print(tableForm)

        return
        colourPointsIndices = [
            index for index in wordIndexList if index < len(self.idToWord)]
        self.visualiser.colourPoints(colourPointsIndices)
        if distanceList[0] == 0.0:
            self.visualiser.colourClicked(wordIndexList[0])

        # Print the tableform of the KNN result
        tableForm = tabulate(zip(wordList, distanceList),
                             headers=['Word', 'Distance'])
        print(tableForm)

    # Start the projection
    def uploadFile(self):
        """Upload a file for the projection/transforming note that for now
        no checks are being done whether the file is valid
        """
        # Select a file
        filePath = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, 'Select File', os.path.expanduser('~\\Documents\\'))[0]
        self.setFile(filePath)

    def setFile(self, filePath, enableKNN=True):
        if filePath == '' or not filePath:
            return
        self.activeFilePath = filePath

        self.serviceManager.startProjection(filePath)
        if enableKNN:
            self.serviceManager.startKNN(filePath)
            self.serviceManager.knnPercentageTimer.start(1000)
        # Timers cant be started by the new thread, so do it here
        self.serviceManager.projectorPercentageTimer.start(1000)
        self.serviceManager.getModelTimer.start(1000)

    def setupKNN(self):
        """Setup the KNN microservice

        Args:
            filePath (string): The path to the used file so that the KNN can access it aswell
        """
        if self.serviceManager.isServiceActive("KNN"):
            return

        # Use a separate thread to prevent GUI hanging
        threading.Thread(target=self.serviceManager.setupKNNs).start()
        self.serviceManager.KNNprogressSignal.connect(
            self.ui.updateKNNPercentage)

    def setDataDirectory(self):
        """For the setting menu, being able to set a default directory for where the Data files are (WIP)
        """
        print("Getting directory")
        filePath = str(QtWidgets.QFileDialog.getExistingDirectory(
            self.ui, "Select Directory"))
        print(filePath)

    def setupProjector(self):
        """Setup the Projector microservice (TODO give it the ability to switch between languages)

        Args:
            filePath (string): The path to the used file
        """
        if self.serviceManager.isServiceActive("Projector"):
            return

        # Use a separate thread to prevent GUI hanging
        threading.Thread(
            target=self.serviceManager.setupProjectors).start()
        self.serviceManager.ProjectorprogressSignal.connect(
            self.ui.updateFittingPercentage)
        self.serviceManager.newProjectorModelSignal.connect(self.addModel)

    def setupTransformer(self):
        """Setup the Transformer microservice (TODO give it the ability to switch between languages, must be the same as the model used though)
        """

        if self.serviceManager.isServiceActive("Transformer"):
            return

        self.visualiser.removeAllPoints()
        # Use a separate thread to prevent GUI hanging
        threading.Thread(target=self.serviceManager.setupTransformers).start()
        self.serviceManager.TransformerprogressSignal.connect(
            self.ui.updateTransformPercentage)
        self.serviceManager.newPointsSignal.connect(self.addToVisualiser)

    def closeWindow(self):
        self.ui.close()

    def addToVisualiser(self, results, words, clusterIDs):
        """Signal from the transformer which has to be forwarded to the UI visualiser

        Args:
            results ([point]): List of points in 2 or 3 dimensions (depending on the projector)
        """
        if len(results) > 0:
            self.visualiser.addPoints(results, words, clusterIDs)

        # Finished
        if len(results) == 0:
            self.visualiser.generateTrees()

    def addModel(self, model, modelName=None):
        """Add a model to the model comboBox

        Args:
            model (bytes): The pickle (for python) of the projector (TODO for cpp)
            modelName (string, optional): For in the case of the loadModel, give it the name. Otherwise generate a name for it. Defaults to None.
        """
        self.models.append(model)
        if not modelName:
            modelName = self.serviceManager.activeService['Projector'] + "Model_v" + str(
                len(self.models))

        self.ui.addModelToList(modelName)

    def getPrecision(self):
        """Python PCA supports a getPrecision function WIP
        """
        self.serviceManager.startValidateProjection(self.activeFilePath)

    # When stop Projection button clicked
    def stopProjection(self):
        self.serviceManager.stopTransformer()


def main():
    """Setup the UI and Provee Object
    """
    app = QtWidgets.QApplication(sys.argv)
    ui = UI(Provee)
    ui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
