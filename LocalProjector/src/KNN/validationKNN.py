import operator
import os
import re
import sys
import time
import grpc
import faiss
import numpy as np
import threading
from PyQt5.QtCore import QObject, QTimer, pyqtSignal, pyqtSlot
from tabulate import tabulate
from concurrent import futures
import src.generated.Python.knn_pb2 as knn
import src.generated.Python.knn_pb2_grpc as rpc
from src.constants import *
from src.utils.helperFunctions import lineToVector, printColoured
import sklearn.neighbors


class ValidationKNN(QObject):
    """The Validation KNN microservice

    Args:
        QObject (QObject): So that we can use QT signal/slots
    """

    def __init__(self):
        """Set constant variables and variables not set by clearVariables.
        Also init other variables by calling clearVariables.
        """
        self.vectorBatchSize = VALIDATIONKNN_BATCH_SIZE
        self.nNeighbours = VALIDATIONKNN_NNEIGBOURS

        self.originalKNNStub = None
        self.transformedPoints = []
        self.updated = False

        super(ValidationKNN, self).__init__()
        self.clearVariables()

    def clearVariables(self):
        """Clear all the variables to default
        """
        self.index = None
        self.sizeSoFar = 0
        self.progress = 0
        self.fileSize = 0
        self.filePath = None
        self.nearestNeighboursModel = None
        self.correctKNNs = [0] * len(self.nNeighbours)
        self.totalKNNs = [0] * len(self.nNeighbours)
        self.running = False

    def addTransformedPoints(self, request, context):
        """Add points from transformerService to buffer to validate later.

        Args:
            request (gRPC): standard gRPC (contains the messages).
            request.vectors ([vector]): contains protoKNN Vector object for each transformed point
            context (gRPC): standard gRPC

        Returns:
            gRPC empty: returns void
        """
        self.transformedPoints += [vector.point for vector in request.vectors]
        self.updated = False
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def clearPoints(self, request, context):
        """Clear transformed points buffer so all previously added points are removed.

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC empty: returns void
        """
        self.transformedPoints = []
        self.updated = False
        printMessage('Cleared points')
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def updateKNN(self):
        """Create a new kNN model on the transformed points.

        Returns:
            [numpy.array([float, float])]: Returns the transformed points used by the model.
                This prevents the ValidationKNN from getting out of sync when more points are added during updateKNN.
        """
        transformed = np.array(self.transformedPoints)
        if self.updated and self.nearestNeighboursModel is not None:
            return transformed

        start_count = time.time()
        printMessage('Update kNN')
        self.nearestNeighboursModel = sklearn.neighbors.NearestNeighbors().fit(transformed)
        printMessage('Finished kNN update in: {0} sec'.format(
            time.time() - start_count))

        self.updated = True
        return transformed

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC request

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return knn.progressKNN(progress=int(self.progress))

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar or self.fileSize is None or self.fileSize == 0:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def validateProjection(self):
        """Calculate the accuracy of the transformed (projected) points compared to the original kNN with the full dimensions.

        Returns:
            {int: float}: contains a amount of nearest neighbours as key and the corresponding accuracy as float (0-100%)
        """
        # Check connection with originalKNN
        if not self.filePath:
            printMessage('Cancel validation, because no filepath was set')
            return None
        # Check connection with originalKNN
        if not self.connectToOriginalKNN():
            printMessage(
                'Cancel validation, because no connection is established to the original kNN')
            return None
        # Check if transformation has run
        if len(self.transformedPoints) == 0:
            printMessage(
                'Cancel validation, because no transformed points have been collected')
            return None

        # Update nearest neighbours and save transformed points in case more are added later
        transformed = self.updateKNN()

        printMessage('Running validation')

        sizeBuffer = 0
        vectorIndex = 0
        vectorBatch = []
        with open(self.filePath, "r", encoding="utf8") as f:
            for line in f:
                if not self.running:
                    printMessage("Stopped running, aborting validation")
                    return None

                sizeBuffer += sys.getsizeof(line)

                _, vector = lineToVector(line)
                vectorBatch.append(vector)

                if len(vectorBatch) >= self.vectorBatchSize and vectorIndex < len(transformed):
                    newIndex = min(len(transformed),
                                   vectorIndex + len(vectorBatch))
                    succesfulComparision = self.compareKNNs(
                        vectorBatch[:newIndex - vectorIndex], transformed[vectorIndex:newIndex])
                    vectorIndex = newIndex
                    vectorBatch = []
                    self.sizeSoFar += sizeBuffer
                    sizeBuffer = 0
                    if not succesfulComparision:
                        printMessage("Aborting validation")
                        return None
                    if vectorIndex >= len(transformed):
                        break
        if len(vectorBatch) > 0 and vectorIndex < len(transformed):
            newIndex = min(len(transformed), vectorIndex + len(vectorBatch))
            succesfulComparision = self.compareKNNs(
                vectorBatch, transformed[vectorIndex:newIndex])
            vectorIndex = newIndex
            self.sizeSoFar += sizeBuffer

        if vectorIndex != len(transformed):
            printMessage('Not all points where validated. There were at least {0} points in the file whilst there were {1} transformed points'.format(
                vectorIndex, len(transformed)))

        accuracyDict = self.calcAccuracy()
        summary = "Finished validation: "
        for nNeighbours, accuracy in accuracyDict.items():
            summary += "\nNumber of neighbours: {0}, Accuracy: {1}%".format(
                nNeighbours, accuracy)
        printMessage(summary)

        self.running = False

        return accuracyDict

    def compareKNNs(self, originalVectors, transformedVectors):
        """Compares the kNNs of a batch of full dimensional vectors to transformed vectors. Compares for multiple k values.

        Args:
            originalVectors ([[float]]): List/array of full dimensional vectors
            transformedVectors ([[float, float]]): List/array of  transformed vectors

        Returns:
            bool: if comparisons were succesful
        """
        for i, nNeighbours in enumerate(self.nNeighbours):
            correctKNNs, totalKNNs = self.compareKNNBatch(
                originalVectors, transformedVectors, nNeighbours)
            if not correctKNNs or not totalKNNs:
                return False
            self.correctKNNs[i] += correctKNNs
            self.totalKNNs[i] += totalKNNs
        return True

    def compareKNNBatch(self, originalVectors, transformedVectors, nNeighbours):
        """Compares the nearest neighbours of a batch of vectors with their full dimensial value and transformed value.

        Args:
            originalVectors ([[float]]): List/array of full dimensional vectors
            transformedVectors ([[float, float]]): List/array of transformed vectors
            nNeighbours (int): Number of nearest neighbours that should be compared per vector (k in kNN).

        Returns:
            (int, int): Amount of correct nearest neighbours found with amount of total nearest neighbours. (None, None) on failure.
        """
        if nNeighbours <= 0:
            printMessage(
                "Invalid nearest neighbours request of {0}NN. Must be at least 1".format(nNeighbours))
            return (None, None)

        originalNNs = self.getOriginalNNBatch(originalVectors, nNeighbours)
        if originalNNs is None:
            return (None, None)
        _, transformedNNs = self.nearestNeighboursModel.kneighbors(
            X=transformedVectors, n_neighbors=nNeighbours)

        if len(originalNNs) != len(transformedNNs):
            printMessage("Retrieved a different amount of originalNNs ({0}) than transformedNNs ({1})".format(
                len(originalNNs), len(transformedNNs)))
            return (None, None)

        correctKNNs = 0
        totalKNNs = 0
        for i in range(len(transformedNNs)):
            correctKNNs += np.sum(np.isin(originalNNs[i], transformedNNs[i]))
            totalKNNs += len(originalNNs[i])
        return (correctKNNs, totalKNNs)

    def getOriginalNNBatch(self, vectors, nNeighbours):
        """Requests the kNN of a batch of full dimensional vectors from the kNN service.

        Args:
            vectors ([[float]]): List/array of full dimensional vectors
            nNeighbours (int): Number of nearest neighbours requested

        Returns:
            np.array([[int]]): Per vector a list of nearest neighbours. Shape = (len(vectors), nNeighbours)
        """
        if not self.originalKNNStub:
            printMessage("Not connected to original kNN")
            return None

        vectorBatchRequest = knn.knnBatchRequest(vectors=[knn.Vector(
            point=vector) for vector in vectors], k=nNeighbours)

        neighboursBatch = self.originalKNNStub.getKNNBatch(vectorBatchRequest)
        originalNNs = np.array([[row.wordIndex for row in neighbour.rows]
                               for neighbour in neighboursBatch.neighbours])
        return originalNNs

    def calcAccuracy(self):
        """Calculates the accuracy of each nearest neighbours comparison.

        Returns:
            {int: float}: contains a amount of nearest neighbours as key and the corresponding accuracy as float (0-100%)
        """
        accuracyDict = {}
        for i, nNeighbours in enumerate(self.nNeighbours):
            accuracy = 0
            if self.totalKNNs[i] > 0:
                accuracy = self.correctKNNs[i] / self.totalKNNs[i] * 100.0
            accuracyDict[nNeighbours] = accuracy
        return accuracyDict

    def getAccuracy(self, request, context):
        """Gets the accuracy per nearest neighbour amount.

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC accuracyKNN: contains a list of nearest neighbours and a list with the corresponding accuracy per nearest neighbour
        """
        accuracyDict = self.calcAccuracy()
        return knn.accuracyKNN(nNeighbours=list(accuracyDict.keys()), accuracy=list(accuracyDict.values()))

    def connectToOriginalKNN(self):
        """Connects to the kNN service to request the nearest neighbours of full dimensional vectors.

        Returns:
            bool: if connecting was succesful
        """
        # Attempt to connect to the gRPC microservice
        if self.originalKNNStub is not None:
            return True

        for _ in range(5):
            try:
                # Try to connect
                channel = grpc.insecure_channel('localhost:50051')
                stub = rpc.KNNStub(channel)
                # Check the 0 progress in order to see if it is running
                stub.getProgress(
                    knn.google_dot_protobuf_dot_empty__pb2.Empty())
                # If connected and running add stub
                self.originalKNNStub = stub
                printMessage("Connected to original kNN")
                return True
            except grpc._channel._InactiveRpcError as e:
                printMessage(
                    "original kNN not running yet, retry after 5 seconds")
            time.sleep(5)

        printMessage("Stopped trying to connect to original kNN")
        return self.originalKNNStub is not None

    def startKNNService(self, request, context):
        """Setup the KNN serive

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC empty: returns void
        """
        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()
        self.filePath = request.path
        self.fileSize = os.path.getsize(self.filePath)
        self.running = True
        # Start up separate thread to prevent hanging
        self.t = threading.Thread(target=self.validateProjection)
        self.t.start()
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def stopKNNService(self, request, context):
        """Stop the current service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC empty: returns void
        """
        self.running = False
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "Validation KNN ", "red")


def serveServer():
    """Setup the GRPC server
    """
    port = '[::]:50056'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_ValidationKNNServicer_to_server(ValidationKNN(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Validation KNN server")
    serveServer()
