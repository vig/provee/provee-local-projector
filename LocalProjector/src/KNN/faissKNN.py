import operator
import os
import re
import sys
import time
import grpc
import faiss
import numpy as np
import threading
from PyQt5.QtCore import QObject, QTimer, pyqtSignal, pyqtSlot
from tabulate import tabulate
from concurrent import futures
import src.generated.Python.knn_pb2 as knn
import src.generated.Python.knn_pb2_grpc as rpc
from src.constants import *
from src.utils.helperFunctions import lineToVector, printColoured


class KNNService(QObject):
    """The KNN microservice

    Args:
        QObject (QObject): So that we can use QT signal/slots
    """
    progressSignal = pyqtSignal(int)

    def __init__(self):
        super(KNNService, self).__init__()
        self.clearVariables()

    def clearVariables(self):
        """Clear all the variables to default (TODO, find more clean way of doing this)
        """
        self.count = 0
        self.index = None
        self.sizeSoFar = 0
        self.progress = 0
        self.running = False
        self.filePath = None

    def stopKNNService(self, request, context):
        """Stop the current service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC void
        """
        self.running = False
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC requset

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return knn.progressKNN(progress=int(self.progress))

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def trainKNN(self):
        """Train the KNN with the given filePath
        """

        # For keeping track of progress (rough estimate)
        self.sizeSoFar = 0
        self.count = 0
        with open(self.filePath, "r", encoding="utf8") as f:
            for line in f:

                # Stop
                if not self.running:
                    return

                self.count += 1
                self.sizeSoFar += sys.getsizeof(line)
                word, vector = lineToVector(line)

                # If the IndexFlatL2 object has not been created yet TODO, use more memory efficient one
                if not self.index:
                    self.index = faiss.IndexFlatL2(len(vector))

                # For keeping track of which index belongs to which word
                self.index.add(np.float32([vector]))
        self.running = False

    def printClosest(self, indices, wordsDict, distances):
        """For debugging, print the results of the KNN request

        Args:
            indices ([[int]]): 2d list of the closest indices
            distances ([[float]]): 2d list of distances to closest words (specified in indices)
        """
        words = [wordsDict[x] for x in indices]
        tableForm = tabulate(list(zip(words, distances[0])), headers=[
                             'Word', 'Distance'])
        printMessage(tableForm)

    def getVectors(self, wordList):
        """Find the vector per word in the given datafile

        Args:
            wordList ([word]): List of used words

        Returns:
            Dict: The dictionary of word, wordVector combinations
        """
        returnDict = {}
        with open(self.filePath, "r", encoding="utf8") as f:
            for line in f:

                # Found all words
                if len(returnDict) == len(wordList):
                    break
                word, vector = lineToVector(line)
                if word in wordList:
                    returnDict[word] = vector
        return returnDict

    def calculateFinalVector(self, sentence):
        """Calculate the semantic request, for example king-man+woman

        Args:
            sentence (string): the linalg expression (or single words)

        Returns:
            resultedVector: The calculated word vector
        """

        # Specify the allowed operations (TODO maybe elementiwse multiplication / division are usefull?)
        ops = {"+": operator.add, "-": operator.sub}

        strippedWord = "".join(sentence.split())
        wordList = re.split('-|\+', strippedWord)

        wordList = [x.lower() for x in wordList]

        wordDict = self.getVectors(wordList)
        if not self.checkWords(wordList, wordDict):
            return np.empty(0, 1)
        orderedOperators = ''.join(c for c in sentence if c in ["-", "+"])

        resultedVector = np.array(wordDict[wordList[0]])

        # Calculate the final vector
        for i in range(1, len(wordList)):
            resultedVector = ops[orderedOperators[i - 1]
                                 ](resultedVector, np.array(wordDict[wordList[i]]))
        return resultedVector

    def getAllDistances(self, request, context):
        requestWord = request.word
        vectorDict = self.getVectors([requestWord])
        if requestWord not in vectorDict:
            printMessage(requestWord + " not in file")
            return knn.distanceList(indices=[], distances=[])

        vector = vectorDict[requestWord]
        distances, indices = self.index.search(np.float32([vector]), int(0.5 * self.count))
        print("Found", len(I[0]), "points")
        return knn.distanceList(indices=indices[0], distances=distances[0])

    # Perform actual KNN
    def knnVector(self, vector, k):
        """Use the index object to extract closest words / distances and

        Args:
            vector ([float]): The calculated word vector
            k (int): How many neighbours we want to

        Returns:
            neighbours: gRPC message containing the words and distances
        """
        D, Indices = self.index.search(np.float32([vector]), k)
        indices = Indices[0]
        distances = D[0]
        resultsDict = {}
        for i in range(len(indices)):
            resultsDict[indices[i]] = distances[i]
        wordsDict = self.getWords(sorted(indices))
        returnRows = []
        for index in indices:
            returnRows.append(
                knn.Row(wordIndex=index, word=wordsDict[index], distance=resultsDict[index]))

        neighbours = knn.Neighbours(rows=returnRows)
        self.printClosest(indices, wordsDict, D)
        return neighbours

    def getWords(self, indicesSorted):
        """Searches and returns the words/labels of indices.

        Args:
            indicesSorted ([int]): indices of vectors of which labels must be found in ascending order

        Raises:
            Exception: throws a exception when requested index was not found in file

        Returns:
            {int: string}: contains the indices as key and requested labels as values
        """
        count = 0
        indices = indicesSorted
        wordList = {}

        with open(self.filePath, "r", encoding="utf8") as f:
            for line in f:
                if count == indices[0]:
                    word, _ = lineToVector(line)
                    wordList[indices[0]] = word
                    indices = indices[1:]

                count += 1

                if len(indicesSorted) == len(wordList):
                    return wordList

        printMessage("WordIndex not in wordList? ERROR")
        raise Exception("WordIndex not in wordList? ERROR")

    def checkWords(self, wordList, wordDict):
        """Check whether all words are found in the file in

        Args:
            wordList ([string]): List of words used in the requested
            wordDict (dict{string,[float]}): List of words found in the file with corresponding vectors

        Returns:
            Bool: Whether all words have been found
        """
        valid = True
        for word in wordList:
            if word not in wordDict:
                printMessage(word + " not in the data file")
                valid = False
        return valid

    def getKNNRequest(self, request, context):
        """Implementation for gRPC message to connect to for the general requests

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grpc Neighbours objects: gRPC message containing the words and distances
        """

        if not self.filePath:
            printMessage("No filePath loaded on KNN")
            return knn.Neighbours(rows=[])

        sentence = request.words
        finalVector = self.calculateFinalVector(sentence)
        # Something wrong with the request, check the output
        if finalVector.shape[0] == 0:
            return knn.Neighbours(rows=[])
        return self.knnVector(finalVector, 10)

    def getKNNBatch(self, request, context):
        """Gets the kNN for a batch of vectors.

        Args:
            request (gRPC): standard gRPC (contains the messages)
            request.vectors ([Vector]): the vectors of which the kNN must be returned
            request.k (int): number of nearest neighbours that should be returned per vector
            context (gRPC): standard gRPC

        Returns:
            NeighboursBatch: NeighboursBatch.neighbours contains Row objects with the nearest neighbours.
                This is in order of the vectors given.
        """
        vectors = [vector.point for vector in request.vectors]
        k = request.k

        distances, indices = self.index.search(np.float32(vectors), k)

        returnNeighbours = []
        for i in range(len(indices)):
            neighbourRows = []
            for j in range(len(indices[i])):
                neighbourRows.append(
                    knn.Row(wordIndex=indices[i, j], word="", distance=distances[i, j]))
            returnNeighbours.append(knn.Neighbours(rows=neighbourRows))
        return knn.NeighboursBatch(neighbours=returnNeighbours)

    def startKNNService(self, request, context):
        """Setup the KNN serive

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC empty: returns void
        """
        printMessage("connected, deleting previous data")
        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()
        self.filePath = request.path
        self.fileSize = os.path.getsize(self.filePath)
        self.running = True
        # Start up separate thread to prevent hanging
        self.t = threading.Thread(target=self.trainKNN)
        self.t.start()
        return knn.google_dot_protobuf_dot_empty__pb2.Empty()


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "KNN", "red")


def serveServer():
    """Setup the GRPC server
    """
    port = '[::]:50051'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_KNNServicer_to_server(KNNService(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the KNN server")
    serveServer()
