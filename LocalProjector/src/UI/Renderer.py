from PyQt5.QtCore import QRunnable, QTimer


# TODO: Currently QRunnable, change this later to something more sensible
class Renderer(QRunnable):
    """Interface from which all renderes inherit

    Args:
        QRunnable (QRunnable): To be able to use signal/slots (Maybe change this back to QObject since we use separate thread)
    """

    def __init__(self, pointRange):
        super(Renderer, self).__init__()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)

    def addPoints(self, points):
        raise NotImplementedError

    def removeAllPoints(self):
        raise NotImplementedError

    # Returns the widget, which can be placed in the GUI
    def getWidget(self):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError

    def downloadDataset(self, filePath):
        raise NotImplementedError
