import sys
import time

import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QRunnable, pyqtSlot
from PyQt5.QtGui import QWidget
from pyqtgraph.Qt import QtCore, QtGui
from src.UI.Renderer import Renderer


class Renderer3D(Renderer):
    """The visualiser which renderes all the points

    Args:
        Renderer (Renderer): The visualier interface
    """

    def __init__(self, pointRange):
        super(Renderer3D, self).__init__(pointRange)
        self.pointRange = pointRange
        self.setupWidget()
        self.setupGrids()
        self.newPoints = np.empty((0, 3))

        # Start the timer specified in the interface
        self.timer.start(1000)

    def setupWidget(self):
        """Create the widget so that the UI can place it
        """
        self.cw = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        self.cw.setLayout(layout)
        self.openGLWidget = gl.GLViewWidget()
        layout.addWidget(self.openGLWidget)

    def addPoints(self, points):
        """Add a list of points to the visualier

        Args:
            points ([points]): List of points
        """
        points = np.array(points)
        if points.shape[1] == 2:
            points = np.append(points, np.zeros((points.shape[0], 1)), axis=1)
        self.newPoints = np.concatenate((self.newPoints, points))

    def getWidget(self):
        """So that the provee object (main) can pass the widget to the UI

        Returns:
            QWidget: The visualiser widget
        """
        return self.cw

    def removeAllPoints(self):
        """Remove all points (TODO We can probably use openGLWidget.clear())
        """
        while(len(self.openGLWidget.items) != 0):
            item = self.openGLWidget.items[0]
            self.openGLWidget.removeItem(item)
            item.deleteLater()
        self.newPoints = np.empty((0, 3))

        self.setupGrids()

    def getGrid(self, rotation, translation):
        """Get the grid GraphicsItem given color, sizes rotations etc.

        Args:
            rotation (Tuple): The x,y,z rotatinos for the grid
            translation (Tuple): The x,y,z translations for the grid

        Returns:
            GLGridItem: The grid Item
        """
        gridSize = 2 * self.pointRange
        spaceSize = 2 * self.pointRange / 10
        gridColor = (255, 255, 255, 100)

        angle, rx, ry, rz = rotation
        tx, ty, tz = translation

        g = gl.GLGridItem()
        g.rotate(angle, rx, ry, rz)
        g.setSize(gridSize, gridSize, gridSize)
        g.setSpacing(spaceSize, spaceSize, spaceSize)
        g.setColor(gridColor)
        g.translate(tx, ty, tz)
        return g

    def downloadDataset(self, filePath):
        """Store the projetion on disk"""
        print("Storing")
        with open(filePath, "w") as file:
            for item in self.openGLWidget.items:
                if isinstance(item, gl.GLScatterPlotItem):
                    for point in item.pos:
                        stringPoint = [str(el) for el in point]
                        file.write(",".join(stringPoint) + "\n")

    def setupGrids(self):
        """Creates the 3 grid axis for x,y,z
        """
        # Three line axis
        axis = gl.GLAxisItem()
        axis.translate(-self.pointRange, -self.pointRange, 0)
        axis.scale(2, 2, 2)
        self.openGLWidget.addItem(axis)

        self.openGLWidget.addItem(self.getGrid(
            (90, 0, 1, 0), (-self.pointRange, 0, self.pointRange)))
        self.openGLWidget.addItem(self.getGrid(
            (90, 1, 0, 0), (0, -self.pointRange, self.pointRange)))
        self.openGLWidget.addItem(self.getGrid((0, 0, 0, 0), (0, 0, 0)))

    def update(self):
        """Every QTimer tick, update the new points, if there are any
        """
        if len(self.newPoints) > 0:
            # TODO: Investigate this further
            QtGui.QApplication.processEvents()
            sp = gl.GLScatterPlotItem(pos=np.asarray(
                self.newPoints), size=2, color=(255, 255, 255, 0.5))
            self.openGLWidget.addItem(sp)
            self.newPoints = np.empty((0, 3))
