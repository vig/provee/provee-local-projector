from PyQt5 import uic, QtWidgets, QtCore, QtGui
from PyQt5.QtGui import QIntValidator
import time
import sys
import pyqtgraph as pg
import os
from .SideGrip import SideGrip
from sklearn.decomposition import IncrementalPCA
from PyQt5.QtWidgets import QTableWidgetItem
# Get GUI_BASE.ui file relative to the position of this script
try:
    scriptFolder = os.path.dirname(os.path.abspath(__file__))
    uiFilePath = os.path.join(scriptFolder, '..', '..', 'GUI_BASE.ui')
    baseUIClass, baseUIWidget = uic.loadUiType(uiFilePath)
except IOError as e:
    print("Could not load GUI_BASE.ui to start UI. Description: \t" + str(e))
# use loaded ui file in the logic class


class UI(baseUIWidget, baseUIClass):
    """The actual UI widgets

    Args:
        baseUIWidget ([type]): QT standard template
        baseUIClass ([type]): QT standard template

    Returns:
        The UI object: so that the Provee object (main) can use it
    """
    _gripSize = 8

    def __init__(self, main, parent=None):
        super(UI, self).__init__(parent)
        # UI related
        self.setupUi(self)
        # self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

        # Add initial model
        self.selectModel.addItem("", 0)

        # Add projector types
        self.addProjectors()

        # Resizing related
        self.oldPos = QtCore.QPoint(0, 0)
        self.sideGrips = [
            SideGrip(self, QtCore.Qt.LeftEdge),
            SideGrip(self, QtCore.Qt.TopEdge),
            SideGrip(self, QtCore.Qt.RightEdge),
            SideGrip(self, QtCore.Qt.BottomEdge),
        ]
        # corner grips should be "on top" of everything, otherwise the side grips
        # will take precedence on mouse events, so we are adding them *after*;
        # alternatively, widget.raise_() can be used
        self.cornerGrips = [QtWidgets.QSizeGrip(self) for i in range(4)]
        self.setupPercentageLoaders()
        self.handleConnections()
        self.setupWordTable()

        self.animatedSeconds.setValidator(QIntValidator())

        # So that UI is subpart of main model
        self.provee = main(self)

    def setupWordTable(self):
        self.wordTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.wordTable.verticalHeader().hide()
        self.wordTable.horizontalHeader().hide()

    def addWordsToTables(self, wordTuples):
        self.clearWordTable()

        if len(wordTuples) == 0:
            return

        rowCount = len(wordTuples)
        self.wordTable.setRowCount(rowCount)
        self.wordTable.setColumnCount(1)

        for rowIndex in range(rowCount):
            word = wordTuples[rowIndex]
            self.wordTable.setItem(rowIndex, 0, QTableWidgetItem(word))

    def clearWordTable(self):
        self.wordTable.setRowCount(0)

    def setVisualiser(self, widget):
        """Put the visualiser in the main stackedwidget, and select it from

        Args:
            widget (QWidget): The visualier
        """
        self.stackedWidget.insertWidget(0, widget)
        self.stackedWidget.setCurrentIndex(0)

    def setupPercentageLoaders(self):
        """Set up the circular percentage loaders
        """
        self.setValue(self.labelPercentageFitting,
                      self.circularProgressFitting, " rgb(115, 185, 255)", 0)
        self.setValue(self.labelPercentageTransforming,
                      self.circularProgressTransforming, " rgba(255, 0, 127, 255)", 0)
        self.setValue(self.labelPercentageKNN,
                      self.circularProgressKNN, "rgb(0, 159, 0)", 0)

    def addProjectors(self):
        self.selectProjector.addItems(["PCA", "UMAP"])

    # Resizing as shown in https://stackoverflow.com/questions/62807295/how-to-resize-a-window-from-the-edges-after-adding-the-property-qtcore-qt-framel
    # IGNORE THIS
    @property
    def gripSize(self):
        return self._gripSize

    def setGripSize(self, size):
        if size == self._gripSize:
            return
        self._gripSize = max(2, size)
        self.updateGrips()

    def updateGrips(self):
        self.setContentsMargins(*[self.gripSize] * 4)
        outRect = self.rect()
        # an "inner" rect used for reference to set the geometries of size grips
        inRect = outRect.adjusted(self.gripSize, self.gripSize,
                                  -self.gripSize, -self.gripSize)
        # top left
        self.cornerGrips[0].setGeometry(
            QtCore.QRect(outRect.topLeft(), inRect.topLeft()))
        # top right
        self.cornerGrips[1].setGeometry(
            QtCore.QRect(outRect.topRight(), inRect.topRight()).normalized())
        # bottom right
        self.cornerGrips[2].setGeometry(
            QtCore.QRect(inRect.bottomRight(), outRect.bottomRight()))
        # bottom left
        self.cornerGrips[3].setGeometry(
            QtCore.QRect(outRect.bottomLeft(), inRect.bottomLeft()).normalized())
        # left edge
        self.sideGrips[0].setGeometry(
            0, inRect.top(), self.gripSize, inRect.height())
        # top edge
        self.sideGrips[1].setGeometry(
            inRect.left(), 0, inRect.width(), self.gripSize)
        # right edge
        self.sideGrips[2].setGeometry(
            inRect.left() + inRect.width(),
            inRect.top(), self.gripSize, inRect.height())
        # bottom edge
        self.sideGrips[3].setGeometry(
            self.gripSize, inRect.top() + inRect.height(),
            inRect.width(), self.gripSize)

    def resizeEvent(self, event):
        QtWidgets.QMainWindow.resizeEvent(self, event)
        self.updateGrips()

    def updateFittingPercentage(self, percentage):
        """Update the Projector progress percentage

        Args:
            percentage (int): percentage
        """
        self.setValue(self.labelPercentageFitting, self.circularProgressFitting,
                      " rgba(115, 185, 255,255)", percentage)

    def updateTransformPercentage(self, percentage):
        """Update the Transformer progress percentage

        Args:
            percentage (int): percentage
        """
        self.setValue(self.labelPercentageTransforming,
                      self.circularProgressTransforming, " rgb(161, 0, 241)", percentage)

    def updateKNNPercentage(self, percentage):
        """Update the KNN progress percentage

        Args:
            percentage (int): percentage
        """
        self.setValue(self.labelPercentageKNN,
                      self.circularProgressKNN, "rgb(0, 159, 0)", percentage)

    def handleConnections(self):
        """Handle the toolbar connections (Maybe transport this to main.py)
        """
        self.btn_minimize.clicked.connect(self.minimizeWindow)
        self.btn_maximize_restore.clicked.connect(self.maximizeWindow)
        self.btn_toggle_menu.clicked.connect(self.clickedMainButton)
        self.settingsButton.clicked.connect(self.swapSettingsScreen)

    def swapSettingsScreen(self):
        """Being able to switch the main screen (between visualiser and config widget (WIP))
        """
        print("Swapping")
        newIndex = (self.stackedWidget.currentIndex() +
                    1) % self.stackedWidget.count()
        self.stackedWidget.setCurrentIndex(newIndex)

    # For the custom top-row buttons

    def minimizeWindow(self):
        """Minimize the window
        """
        self.showMinimized()
        self.updateGrips()

    def clickedMainButton(self):
        """Disable/enable the left menu
        """
        print("clicked main")
        self.frame_left_menu.setVisible(not self.frame_left_menu.isVisible())

    def maximizeWindow(self):
        """Maximize the window
        """
        self.showMaximized()
        self.updateGrips()

    def closeWindow(self):
        """Close the window
        """
        self.close()

    # Setting the percentage values
    def setValue(self, labelPercentage, progressBarName, color, value):
        """Set the value of a circular progress bar (KNN, Projector or Transformer)

        Args:
            labelPercentage (int): New Percentage
            progressBarName (QWidget): The widget of the progress Bar
            color (string): The used color
            value (int): The new progress percentage
        """
        s = 3
        # CONVERT VALUE TO INT
        value = round(value)
        htmlText = """<p align="center"><span style=" font-size:20pt;">{VALUE}</span><span style=" font-size:15pt; vertical-align:super;">%</span></p>"""

        # HTML TEXT PERCENTAGE
        labelPercentage.setText(htmlText.replace("{VALUE}", str(value)))

        # CALL DEF progressBarValue
        self.progressBarValue(value, progressBarName, color)

    def progressBarValue(self, value, widget, color):
        """Calculate the angle of the circular loading and set it

        Args:
            value (int): New Percentage
            widget (QWidget): The widget of the progress Bar
            color (string): The used color
        """
        # PROGRESSBAR STYLESHEET BASE
        styleSheet = """
        QFrame{
            border-radius: 50px;
            background-color: qconicalgradient(cx:0.5, cy:0.5, angle:90, stop:{STOP_1} rgba(85, 85, 127, 100), stop:{STOP_2} {COLOR});
            }
        """

        # GET PROGRESS BAR VALUE, CONVERT TO FLOAT AND INVERT VALUES
        # stop works of 1.000 to 0.000
        progress = (100 - value) / 100.0

        # GET NEW VALUES
        stop_1 = str(progress - 0.001)
        stop_2 = str(progress)

        # FIX MAX VALUE
        if value == 100:
            stop_1 = "1.000"
            stop_2 = "1.000"

        # SET VALUES TO NEW STYLESHEET
        newStylesheet = styleSheet.replace("{STOP_1}", stop_1).replace(
            "{STOP_2}", stop_2).replace("{COLOR}", color)

        # APPLY STYLESHEET WITH NEW VALUES
        widget.setStyleSheet(newStylesheet)

    # Default visualisation

    # Custom dragging window
    def mousePressEvent(self, event):
        self.oldPos = event.globalPos()

    def mouseMoveEvent(self, event):
        if self.frame_label_top_btns.underMouse():
            delta = QtCore.QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()

    def addModelToList(self, name):
        self.selectModel.addItem(name)
