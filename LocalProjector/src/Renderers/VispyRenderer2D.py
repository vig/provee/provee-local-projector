import sys
import numpy as np

from vispy import scene, app
from src.Renderers.BaseRenderer import BaseRenderer
import vispy
from random import randrange
import threading
from PyQt5.QtCore import pyqtSignal
import math

pointClickedColour = (1, 0, 0, 1)
pointKNNColour = (0, 0, 1, 1)


class VispyRenderer(BaseRenderer):

    foundPointSignal = pyqtSignal(int)

    def __init__(self, pointRange):
        super(VispyRenderer, self).__init__(pointRange)
        self.pointRange = pointRange

        self.setupWidget()
        self.lastCount = 1
        self.timer.start(1000)
        self.colors = np.empty((0, 4), dtype=np.float32)
        self.updateRender = False
        self.foundPointSignal.connect(self.pointFound)
        self.lastMousePos = None

    def pointFound(self, index):
        """Changes colour of the clicked points

        Args:
            index (int): the index of the point
        """
        print("found index", index)
        self.colors[index] = pointClickedColour
        self.updateRender = True

    def setupWidget(self):
        """Sets up the widget
        """
        self.canvas = scene.SceneCanvas(
            keys='interactive', size=(600, 600), show=True)
        self.canvas.events.mouse_release.connect(self.on_mouse_release)
        self.canvas.events.mouse_press.connect(self.mouse_press)

        self.setupGrid()
        self.scatter = scene.visuals.Markers()
        self.view.add(self.scatter)

    def get_nearest_index(self, p0):
        """Calculates nearest index of clicked: TODO kd tree

        Args:
            p0 (point): the clicked position
        """
        if(len(self.data)) == 0:
            return
        count = 0
        dist = 100000
        ind = 0
        for i in range(len(self.data)):
            d = (self.data[i][0] - p0[0]) ** 2 + (self.data[i][1] - p0[1]) ** 2
            if d < dist:
                ind = i
                dist = d

        self.foundPointSignal.emit(ind)

    def colourClicked(self, index):
        """Changes the color of clicked points

        Args:
            index (int): The index of the point
        """
        self.colors[index] = pointClickedColour

    def clearColours(self):
        """Removes all colour
        """
        prevLength = self.colors.shape[0]
        self.colors = np.ones((prevLength, 4))
        self.updateRender = True

    def mouse_press(self, event):
        """Updates last position of

        Args:
            event (MouseEvent): the event
        """
        self.lastMousePos = event.pos

    def on_mouse_release(self, event):
        """Finds where was clicked in

        Args:
            event (Mouseevent): The click event
        """
        print("Distance", distance(event.pos, self.lastMousePos))
        if distance(event.pos, self.lastMousePos) > 2:
            return

        self.clearColours()

        tr = self.canvas.scene.node_transform(self.view.scene)
        point = tr.map(event.pos)
        print(event.pos)
        t = threading.Thread(
            target=self.get_nearest_index, args=(point,))
        t.start()

    def getWidget(self):
        """Returns the widget to the service manager

        Returns:
            widget: The widget
        """
        return self.canvas.native

    def getGrids(self):
        """Creates two grids

        Returns:
            grid, grid: the two grids
        """
        xaxis = scene.AxisWidget(orientation='bottom',
                                 axis_label='X Axis',
                                 axis_font_size=12,
                                 axis_label_margin=50,
                                 tick_label_margin=5)

        yaxis = scene.AxisWidget(orientation='left',
                                 axis_label='Y Axis',
                                 axis_font_size=12,
                                 axis_label_margin=50,
                                 tick_label_margin=5)

        return xaxis, yaxis

    def setupGrid(self):
        """Sets up the grid
        """
        grid = self.canvas.central_widget.add_grid(margin=10)
        grid.spacing = 0

        title = scene.Label("Plot Title", color='white')
        title.height_max = 40
        grid.add_widget(title, row=0, col=0, col_span=2)

        xaxis, yaxis = self.getGrids()
        yaxis.width_max = 80
        grid.add_widget(yaxis, row=1, col=0)
        xaxis.height_max = 80
        grid.add_widget(xaxis, row=2, col=1)

        right_padding = grid.add_widget(row=1, col=2, row_span=1)
        right_padding.width_max = 50

        self.view = grid.add_view(row=1, col=1, border_color='white')

        xaxis.link_view(self.view)
        yaxis.link_view(self.view)

    def removeAllPoints(self):
        """Removes all the points
        """
        self.data = np.empty((0, 2))
        self.colors = np.empty((0, 4), dtype=np.float32)

    def colourPoints(self, pointsIndices):
        """Colours the Points

        Args:
            pointsIndices ([point]): points
        """
        for index in pointsIndices:
            self.colors[index] = pointKNNColour
            self.updateRender = True

    def addPoints(self, points):
        """Add points and colours to

        Args:
            points ([point]): the points
        """
        self.data = np.concatenate((self.data, np.array(points)))
        newColors = [(1.0, 1.0, 1.0, 1.0)] * len(points)
        self.colors = np.concatenate((self.colors, np.asarray(newColors)))

    def update(self):
        if self.lastCount == len(self.data) and not self.updateRender:
            return
        self.updateRender = False

        if len(self.data) == 0:
            self.scatter.set_data(self.data, size=8, edge_width=1)
            return
        print("drawing")

        self.scatter.set_data(
            self.data, size=8, face_color=self.colors, edge_width=1)
        self.lastCount = len(self.data)
        print("Plotted", self.lastCount, "points")


def distance(p0, p1):
    """Calculate distance of

    Args:
        p0 (point): point 1
        p1 (point): point 2
    Returns:
        float: distance
    """
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)
