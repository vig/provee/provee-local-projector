import sys
import numpy as np
from sklearn.neighbors import KDTree
from vispy import scene, app
from src.Renderers.BaseRenderer import BaseRenderer
import vispy
from random import randrange
import threading
from PyQt5.QtCore import pyqtSignal
import math
import time
from fast_histogram import histogram2d
from PyQt5.QtCore import QObject, QTimer
from vispy.visuals.transforms import STTransform
import string
import sklearn
from src.utils.helperFunctions import distance
from vispy.scene.visuals import Text
import marisa_trie
import seaborn as sns


class DensityRenderer(BaseRenderer):
    """The main renderer
    """

    finishedDensity = pyqtSignal(object, object)
    newMaxDensity = pyqtSignal(int)

    def __init__(self, pointRange):
        super(DensityRenderer, self).__init__(pointRange)
        self.lastCount = 1
        self.pointRange = pointRange
        self.setupUI()

        self.text = None
        self.renderingMode = 0

        self.minVal = 0
        self.maxVal = 100

        self.densityBinFactor = 4

        self.maxAnnotations = 200

        # Setup cleared variables
        self.removeAllPoints()

        self.lastMousePos = None

        self.newMaxDensity.connect(self.setMaxValue)
        self.timer.start(1000)

        self.animatedMovementTimer = QTimer()
        self.animatedMovementTimer.timeout.connect(self.animatedMoveCamera)
        self.animatedIteration = 0

    def setupUI(self):
        self.setupWidget()
        self.rangeSlider.startValueChanged.connect(self.minValueChanged)
        self.rangeSlider.endValueChanged.connect(self.maxValueChanged)

    def switchToDensity(self):
        self.renderingMode = 1
        self.update(True)

    def switchToDensityRequest(self):
        self.renderingMode = 2
        self.update(True)

    def switchToNormal(self):
        self.renderingMode = 0
        self.update(True)

    def setMaxValue(self, maxVal):
        self.rangeSlider.setMax(maxVal)

    def changeRangeSlider(self, minVal, maxVal):
        self.rangeSlider.setRange(minVal, maxVal)

    def getWidget(self):
        return self.mainFrame

    def mouse_press(self, event):
        self.lastMousePos = event.pos

    def on_mouse_release(self, event):
        """Finds where was clicked in

        Args:
            event (Mouseevent): The click event
        """
        if not self.kdTree:
            return

        # Too far away
        if distance(event.pos, self.lastMousePos) > 2:
            return

        tr = self.canvas.scene.node_transform(self.view.scene)
        point = tr.map(event.pos)

        _, indice = self.kdTree.query([point[:2]], 1)
        self.clickedPoint = indice[0][0]
        self.foundWordIndex = None
        self.update(True)

    def removeAllPoints(self):
        """Remove all the points"""
        self.data = np.empty((0, 2))
        self.clusterIDs = np.array([])

        self.kdTree = None
        self.clickedPoint = None

        self.wordTrie = None
        self.words = []
        self.foundWordIndex = None
        self.foundWordIndices = []

        self.densityRequestIndices = None
        self.densityRequestDistances = None

        self.update(True)

    def minValueChanged(self, val):
        """min Value of the rangeslider
        """
        self.minVal = val

    def maxValueChanged(self, val):
        """max Value of the rangeslider
        """
        self.maxVal = val

    def goToWordAnimated(self, table, clickedItem, animatedSeconds):
        """Move to the word with an animation
        """
        row = clickedItem.row()

        index = self.foundWordIndices[row]
        self.foundWordIndex = index
        self.clickedPoint = None
        self.newPosition = self.data[index]
        self.update(True)

        if animatedSeconds <= 0:
            # Set and update camera
            self.view.camera.center = tuple(self.newPosition)
            self.view.camera.view_changed()
        else:
            # Run animation
            self.animatedFPS = 60
            self.animatedMaxSteps = animatedSeconds * self.animatedFPS
            self.animatedInterval = 1000 // self.animatedFPS

            self.oldPosition = list(self.view.camera.center)[:2]
            self.animatedIteration = 0
            self.animatedMovementTimer.start(self.animatedInterval)

    def animatedMovementFunction(self, iteration):
        # https://stats.stackexchange.com/questions/214877/is-there-a-formula-for-an-s-shaped-curve-with-domain-and-range-0-1

        if iteration == 0.0:
            return 0

        if iteration == self.animatedMaxSteps:
            return 1

        relativeIteration = iteration / self.animatedMaxSteps
        beta = 3
        temp = pow(relativeIteration / (1 - relativeIteration), -1 * beta)

        return 1 / (1 + temp)

    def animatedMoveCamera(self):
        """Move the camera in an animation
        """
        directionVector = self.newPosition - self.oldPosition
        r = self.animatedMovementFunction(self.animatedIteration)
        currentPosition = self.oldPosition + r * directionVector
        self.view.camera.center = tuple(currentPosition)
        self.view.camera.view_changed()

        self.animatedIteration += 1

        if self.animatedIteration == self.animatedMaxSteps:
            self.animatedMovementTimer.stop()

    def findWord(self, word):
        """Find all prefixes from a word
        """
        if not self.wordTrie:
            print("Tree not generated yet")
            return [], []

        wordList = self.wordTrie.keys(word)
        if len(wordList) == 0:
            print(word, "not in the projected set")
            return [], []

        indices = []
        prevKey = None
        for key in wordList:
            if key != prevKey:
                indices += self.wordTrie[key]
            prevKey = key

        self.foundWordIndices = indices
        self.update(True)
        return wordList, indices

    def addPoints(self, points, words, clusterIDs):
        """Store the points and words
        """
        self.data = np.concatenate((self.data, np.array(points)))
        self.clusterIDs = np.concatenate(
            (self.clusterIDs, np.array(clusterIDs)))

        # Perhaps try later to improve this
        self.words = self.words + words

    def generateTrees(self):
        """Generate the tree and the trie
        """
        print("Generating tries")
        tuples = [(el,) for el in range(len(self.words))]
        self.wordTrie = marisa_trie.RecordTrie("ix", zip(self.words, tuples))
        self.kdTree = sklearn.neighbors.KDTree(self.data)

    def getPointsVisible(self):
        if not self.kdTree:
            return np.empty((0, 0))

        size = max(self.view.camera.rect.right - self.view.camera.rect.left,
                   self.view.camera.rect.top - self.view.camera.rect.bottom)
        indices = self.kdTree.query_radius(
            [self.view.camera.rect.center], r=size)[0]
        return indices

    def storeDensityRequestResults(self, indices, distances):

        if not indices or not distances:
            return

        self.densityRequestIndices = indices
        self.densityRequestDistances = distances
        print("Rendered ", len(self.data))
        selectedData = self.data[indices]
        x = selectedData[:, 0]
        y = selectedData[:, 1]

        ymin = self.view.camera.rect.bottom
        ymax = self.view.camera.rect.top
        xmin = self.view.camera.rect.left
        xmax = self.view.camera.rect.right

        nx, ny = self.canvas.size
        self.bins = (nx // self.densityBinFactor, ny // self.densityBinFactor)
        distances = np.clip(distances, 0, np.mean(distances))
        normalizedDistances = distances - np.min(distances)
        normalizedDistances = normalizedDistances / np.max(normalizedDistances)
        normalizedDistances = 1 - normalizedDistances

        array = histogram2d(y, x, bins=self.bins,
                            range=((ymin, ymax), (xmin, xmax)), weights=normalizedDistances)

        self.densityRequestImage.set_data(array)
        self.densityRequestImage.clim = (np.min(array), np.max(array))

    def drawDensityRequest(self):
        self.densityRequestImage.transform = STTransform(translate=[self.view.camera.rect.left, self.view.camera.rect.bottom], scale=[
            self.view.camera.rect.width / self.bins[1], self.view.camera.rect.height / self.bins[0]])
        self.densityRequestImage.cmap = "hot"
        self.densityRequestImage.interpolation = "gaussian"

    def getDensity(self, data):
        """Calculate the density of the current shown points in

        Args:
            data ([points]): the data to check if its in the view

        Returns:
            the positions of each gridCell, the density array and the used scale
        """
        x = data[:, 0]
        y = data[:, 1]

        ymin = self.view.camera.rect.bottom
        ymax = self.view.camera.rect.top
        xmin = self.view.camera.rect.left
        xmax = self.view.camera.rect.right

        nx, ny = self.canvas.size
        self.bins = (nx // self.densityBinFactor, ny // self.densityBinFactor)
        array = histogram2d(y, x, bins=self.bins,
                            range=((ymin, ymax), (xmin, xmax)))

        maxVal = int(np.max(array))

        self.newMaxDensity.emit(maxVal)
        return array

    def drawDensity(self, data):
        """Draw the density plot
        """
        arr = self.getDensity(data)
        arr = np.clip(arr, 0, self.maxVal)

        self.densityImage.set_data(arr)
        self.densityImage.transform = STTransform(translate=[self.view.camera.rect.left, self.view.camera.rect.bottom], scale=[
            self.view.camera.rect.width / self.bins[1], self.view.camera.rect.height / self.bins[0]])
        self.densityImage.cmap = "hot"
        self.densityImage.interpolation = "gaussian"
        self.densityImage.clim = (np.min(arr), np.max(arr))

    def getColours(self):
        """Get the correct amount of colours
        """
        colourAmount = len(set(self.clusterIDs))
        palette = sns.color_palette(None, colourAmount)

        colours = [palette[int(clusterID % colourAmount)]
                   for clusterID in self.clusterIDs]

        # If only 1 colour, use white
        if colourAmount == 1:
            colours = [(1, 1, 1, 1)] * len(self.clusterIDs)
        return colours

    def getSizes(self):
        """Get all the sizes
        """
        sizes = np.repeat(5, len(self.data))

        for index in self.foundWordIndices:
            sizes[index] = 12

        if self.foundWordIndex:
            sizes[self.foundWordIndex] = 20

        if self.clickedPoint:
            sizes[self.clickedPoint] = 20

        return sizes

    def showDensityScreen(self, data):
        """Show the density screen
        """
        self.drawDensity(data)
        self.scatter.set_data(np.empty((0, 2)))

    def showDensityRequestScreen(self, data):
        """Show the Density request plot (if available)
        """
        self.storeDensityRequestResults(
            self.densityRequestIndices, self.densityRequestDistances)
        self.drawDensityRequest()
        self.scatter.set_data(np.empty((0, 2)))

    def showHomeScreen(self, data):
        """Show the scatterplot
        """
        if(len(data) == 0):
            self.scatter.set_data(
                data, size=5, face_color=(1, 1, 1, 1), edge_width=0)
        else:
            colours = self.getColours()
            sizes = self.getSizes()
            self.scatter.set_data(
                data, size=sizes, face_color=colours, edge_width=0)
        self.lastCount = len(data)

    def drawText(self):
        indices = self.getPointsVisible()
        if indices.shape == (0, 0):
            return

        if self.text:
            self.text.parent = None

        if(len(indices) > self.maxAnnotations):
            self.text = None
            return

        words = [self.words[index] for index in indices]
        positions = self.data[indices]

        self.text = Text(text=words, pos=positions,
                         parent=self.view.scene, color='red')

    def drawPoints(self, force):
        """Draw the points and the density plot
        """
        renderingFunctions = [self.showHomeScreen,
                              self.showDensityScreen, self.showDensityRequestScreen]
        # Clear the plot
        self.densityImage.transform = STTransform(scale=[0])
        self.densityRequestImage.transform = STTransform(scale=[0])

        renderingFunctions[self.renderingMode](self.data)

    def update(self, force=False):
        """Main update function
        """

        self.drawText()
        force |= self.renderingMode != 0
        if self.lastCount == len(self.data) and not force:
            return

        if len(self.data) == 0 and not force:
            self.squares.set_data(np.empty((0, 2)))
            return

        self.drawPoints(force)
