import sys
import time

import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl  # for 3D plots
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QRunnable, pyqtSlot
from PyQt5.QtGui import QWidget
from pyqtgraph.Qt import QtCore, QtGui
from src.Renderers.BaseRenderer import BaseRenderer


class Renderer2D(BaseRenderer):
    """The visualiser which renderes all the points

    Args:
        Renderer (Renderer): The visualier interface
    """

    def __init__(self, pointRange):
        super(Renderer2D, self).__init__(pointRange)
        self.pointRange = pointRange
        self.setupWidget()
        self.setupGrids()
        self.newPoints = np.empty((0, 2))  # Concatenate points

        # Start the timer specified in the interface
        self.timer.start(1000)

    def setupWidget(self):
        """Create the widget so that the UI can place it
        """
        self.cw = QtGui.QWidget()  # Define a top-level widget to hold everything
        layout = QtGui.QVBoxLayout()
        self.cw.setLayout(l)
        # PlotWidget: 2D equivalent of 3D GLviewwidget? Or ViewBox
        self.View_Box = pg.PlotWidget()
        layout.addWidget(self.View_Box)   # Add the Widget

    def addPoints(self, points):
        """Add a list of points to the visualier

        Args:
            points ([points]): List of points
        """
        self.newPoints = np.concatenate((self.newPoints, np.array(points)))

    def getWidget(self):
        """So that the provee object (main) can pass the widget to the UI

        Returns:
            QWidget: The visualiser widget
        """
        return self.cw

    def removeAllPoints(self):
        """Remove all points (TODO We can probably use openGLWidget.clear())
        """
        # Does this work when not every individual grid is added as an item?
        while(len(self.View_Box.getPlotItem().items) != 0):
            item = self.View_Box.getPlotItem().items[0]
            self.View_Box.getPlotItem().removeItem(item)
            item.deleteLater()  # delete in a safe way
        self.newPoints = np.empty((0, 3))

        self.setupGrids()

    def setupGrids(self):
        """Creates the 3 grid axis for x,y,z
        """
        self.View_Box.showGrid(
            x=True, y=True)  # Choose whether to show grids or not
        self.View_Box.setXRange(-self.pointRange, self.pointRange, 0)
        self.View_Box.setXRange(-self.pointRange, self.pointRange, 0)

    def update(self):
        """Every QTimer tick, update the new points, if there are any
        """
        if len(self.newPoints) > 0:
            # TODO: Investigate this further
            QtGui.QApplication.processEvents()
            sp = pg.ScatterPlotItem(pos=np.asarray(self.newPoints), size=2, color=(
                255, 255, 255, 0.5), hoverable=True)
            self.View_Box.addItem(sp)  # Add a new scatterplot each time.
            self.newPoints = np.empty((0, 2))
