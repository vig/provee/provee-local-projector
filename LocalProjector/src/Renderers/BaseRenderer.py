from PyQt5.QtCore import QObject, QTimer
import numpy as np
from vispy import scene
from PyQt5.QtWidgets import QFrame, QVBoxLayout
from src.UI.rangeslider import QRangeSlider
# TODO: Currently QRunnable, change this later to something more sensible


class BaseRenderer(QObject):
    """Interface from which all renderes inherit

    Args:
        QRunnable (QRunnable): To be able to use signal/slots (Maybe change this back to QObject since we use separate thread)
    """

    def __init__(self, pointRange):
        super(BaseRenderer, self).__init__()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.clusterIDs = np.array([])
        self.data = np.empty((0, 2))

    def downloadDataset(self, filepath):
        """Store the projected dataset to memory

        Args:
            filepath (string): filePath
        """

        # Needs update
        if len(self.data) == 0:
            print("Empty dataset")
            return

        with open(filepath, "w", encoding="utf-8") as f:
            for line in zip(self.wordTrie.keys(), self.data):
                word = line[0]
                string = " ".join([word] + [str(el) for el in line[1]]) + "\n"
                f.write(string)

    def uploadProjectionData(self, filepath):
        """Upload a projected data set
        """

        self.removeAllPoints()

        allPoints = []
        allWords = []
        with open(filepath, "r", encoding="utf-8") as f:
            lines = f.readlines()

            for line in lines:
                splitLine = line.split(" ")
                word = splitLine[0]

                pointLine = [float(el) for el in splitLine[1:]]
                allPoints.append(pointLine)
                allWords.append(word)

        clusterIDs = np.repeat(0, len(allPoints))

        self.addPoints(allPoints, allWords, clusterIDs)
        self.generateTrees()

    def addPoints(self, points):
        raise NotImplementedError

    def removeAllPoints(self):
        raise NotImplementedError

    # Returns the widget, which can be placed in the GUI
    def getWidget(self):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError

    def getGrids(self):
        """Creates two grids

        Returns:
            grid, grid: the two grids
        """
        xaxis = scene.AxisWidget(orientation='bottom',
                                 axis_label='X Axis',
                                 axis_font_size=12,
                                 axis_label_margin=50,
                                 tick_label_margin=5)

        yaxis = scene.AxisWidget(orientation='left',
                                 axis_label='Y Axis',
                                 axis_font_size=12,
                                 axis_label_margin=50,
                                 tick_label_margin=5)

        return xaxis, yaxis

    def setupGrid(self):
        """Sets up the grid
        """
        grid = self.canvas.central_widget.add_grid(
            margin=10, bgcolor=(0, 0, 0, 1))
        grid.spacing = 0

        title = scene.Label("Plot Title", color='white')
        title.height_max = 40
        grid.add_widget(title, row=0, col=0, col_span=2)

        xaxis, yaxis = self.getGrids()
        yaxis.width_max = 80
        grid.add_widget(yaxis, row=1, col=0)
        xaxis.height_max = 80
        grid.add_widget(xaxis, row=2, col=1)

        right_padding = grid.add_widget(row=1, col=2, row_span=1)
        right_padding.width_max = 50

        self.view = grid.add_view(row=1, col=1, border_color='white')
        self.view.camera = scene.PanZoomCamera()
        xaxis.link_view(self.view)
        yaxis.link_view(self.view)

    def setupWidget(self):
        """Sets up the widget
        """
        self.canvas = scene.SceneCanvas(
            keys='interactive', size=(600, 600), show=False)
        self.canvas.events.mouse_release.connect(self.on_mouse_release)
        self.canvas.events.mouse_press.connect(self.mouse_press)
        self.setupGrid()
        self.scatter = scene.visuals.Markers()
        self.squares = scene.visuals.Markers()
        self.densityImage = scene.visuals.Image()
        self.densityRequestImage = scene.visuals.Image()
        self.view.add(self.squares)
        self.view.add(self.scatter)
        self.view.add(self.densityImage)
        self.view.add(self.densityRequestImage)

        self.mainFrame = QFrame()
        self.rangeSlider = QRangeSlider()

        layout = QVBoxLayout()
        layout.addWidget(self.canvas.native)
        layout.addWidget(self.rangeSlider)
        self.rangeSlider.setMaximumHeight(20)
        self.mainFrame.setLayout(layout)
