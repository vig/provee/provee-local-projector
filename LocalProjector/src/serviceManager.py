import grpc
import src.generated.Python.knn_pb2_grpc as knnRpc
import src.generated.Python.knn_pb2 as knn
import src.generated.Python.projector_pb2_grpc as projRpc
import src.generated.Python.projector_pb2 as proj
import src.generated.Python.transformer_pb2 as trans
import src.generated.Python.transformer_pb2_grpc as transRpc
from PyQt5.QtCore import QObject, QTimer, pyqtSignal, QThread
import time
import pickle
import copy
import threading
import atexit
import subprocess
import os


class ServiceManager(QObject):
    """Handles the connections to the microservices (KNN,Projector,Transformer)

    Args:
        QObject (QObject): So that we can use signals/slots
    """

    # The signals to which the Provee Object (main) can connect
    KNNprogressSignal = pyqtSignal(int)
    densityRequestSignal = pyqtSignal(object, object)
    ProjectorprogressSignal = pyqtSignal(int)
    TransformerprogressSignal = pyqtSignal(int)
    newProjectorModelSignal = pyqtSignal(bytes)
    newPointsSignal = pyqtSignal(object, object, object)
    knnRequestSignal = pyqtSignal(object, object, object)

    def __init__(self):
        """Setup timers and set services to None.
        """
        super(ServiceManager, self).__init__()

        # Setup the timers to retrieve information from the microservices (start them when connections are being made)
        self.knnPercentageTimer = QTimer()
        self.knnPercentageTimer.timeout.connect(self.getKNNProgress)

        self.projectorPercentageTimer = QTimer()
        self.projectorPercentageTimer.timeout.connect(
            self.getProjectorProgress)

        self.getModelTimer = QTimer()
        self.getModelTimer.timeout.connect(self.getProjectorModel)

        self.transformerPercentageTimer = QTimer()
        self.transformerPercentageTimer.timeout.connect(
            self.getTransformerProgress)

        self.stubs = {"Projector": {}, "Transformer": {}, "KNN": {}}
        self.activeService = {"Projector": None,
                              "Transformer": None, "KNN": None}

    def isServiceActive(self, serviceType):
        """Gives the current active service of a service type.

        Args:
            serviceType (string): service type as defined as keys in self.stubs

        Returns:
            gRPC stub: the stub of the requested service
        """
        return self.activeService[serviceType] in self.stubs[serviceType]

    def setupService(self, serviceType, name, scriptPath, channelAddress, stubFunc, serviceProtos):
        """Create a separate process for the projector (gRPC)

        Args:
            filePath (string): The path to the file containing the data embedding (which we want to project)
        """

        # If stub was already added and setup, skip
        if name in self.stubs[serviceType]:
            return

        # Start new process
        process = subprocess.Popen(
            ['python', '-m', scriptPath])
        # Close process when the main process stops/crashes
        atexit.register(process.terminate)

        # Attempt to connect to the gRPC microservice
        for _ in range(20):
            try:
                # Try to connect
                channel = grpc.insecure_channel(channelAddress)
                stub = stubFunc(channel)
                # Check the 0 progress in order to see if it is running
                stub.getProgress(
                    serviceProtos.google_dot_protobuf_dot_empty__pb2.Empty())
                # If connected and running add stub
                self.stubs[serviceType][name] = stub
                return
            except grpc._channel._InactiveRpcError as e:
                print(serviceType, name,
                      "not running yet, retry after 3 seconds")
            time.sleep(3)

        if name not in self.stubs[serviceType]:
            print("Stopped trying to connect to", serviceType, name)

    def setupKNNs(self):
        """Setup kNN microservices.
        """
        self.setupService('KNN', 'faiss', 'src.KNN.faissKNN',
                          'localhost:50051', knnRpc.KNNStub, knn)
        self.activeService['KNN'] = 'faiss'
        self.setupService('KNN', 'validation', 'src.KNN.validationKNN',
                          'localhost:50056', knnRpc.ValidationKNNStub, knn)
        self.newPointsSignal.connect(self.addPointsToValidationKNN)

    def setupProjectors(self):
        """Setup projector microservices.
        """
        self.setupService('Projector', 'PCA', 'src.Projections.Python.Projector.PCA.PCAProjector',
                          'localhost:50052', projRpc.ProjectorStub, proj)
        self.activeService['Projector'] = 'PCA'
        self.setupService('Projector', 'UMAP', 'src.Projections.Python.Projector.UMAP.UMAPProjector',
                          'localhost:50054', projRpc.ProjectorStub, proj)

    def setupTransformers(self):
        """Start up the transformer microservices.
        """
        self.setupService('Transformer', 'PCA', 'src.Projections.Python.Transformer.ProjectionTransformer',
                          'localhost:50053', transRpc.TransformerStub, trans)
        self.activeService['Transformer'] = 'PCA'
        self.setupService('Transformer', 'UMAP', 'src.Projections.Python.Transformer.UMAPTransformer',
                          'localhost:50055', transRpc.TransformerStub, trans)

    def startProjection(self, filePath):
        """Starts the projection on the active projector service.

        Args:
            filePath (string): The path to the file containing the data embedding (which we want to project)
        """
        if not self.isServiceActive('Projector'):
            return

        self.stubs["Projector"][self.activeService['Projector']].stopProjectorService(
            proj.google_dot_protobuf_dot_empty__pb2.Empty())
        self.stubs["Projector"][self.activeService['Projector']].startProjectorService(
            proj.startProjector(path=filePath))

    def startKNN(self, filePath):
        """Actually start the KNN service for a file

        Args:
            filePath (string): Path to the file with embeddings
        """
        if not self.isServiceActive("KNN"):
            return

        self.stubs["KNN"][self.activeService['KNN']].stopKNNService(
            knn.google_dot_protobuf_dot_empty__pb2.Empty())
        self.stubs["KNN"][self.activeService['KNN']
                          ].startKNNService(knn.startKNN(path=filePath))

    def startTransformer(self, filePath, m):
        """Gets the projection (gRPC)

        Args:
            filePath (string): The path to the file we want to transform (note doesnt have to be the same one as the one which the model was projected on (WIP))
            m (bytes): the pickle result for python of the model
        """
        self.t = threading.Thread(target=self.getPoints, args=(filePath, m,))
        self.t.start()

    def startValidateProjection(self, filePath):
        """Starts validation kNN service to validate the projection.

        Args:
            filePath (string): Path to the file with embeddings
        """
        if not self.isServiceActive("KNN"):
            return

        self.stubs['KNN']['validation'].stopKNNService(
            knn.google_dot_protobuf_dot_empty__pb2.Empty())
        self.stubs['KNN']['validation'].startKNNService(
            knn.startKNN(path=filePath))

    def getPoints(self, filePath, m):
        """Slot for the getPoints timer, retrieves the points from the transformer

        Args:
            filePath (string): The path to the file we want to transform (note doesnt have to be the same one as the one which the model was projected on (WIP))
            m (bytes): the pickle result for python of the model
        """
        if not self.isServiceActive("Transformer"):
            return

        self.stopTransformer()
        self.clearValidationKNN()

        pointBuffer = []
        for pointChunk in self.stubs["Transformer"][self.activeService["Transformer"]].startTransformerService(trans.startTransformer(path=filePath, model=m)):
            pointBuffer = []
            wordBuffer = []
            clusterBuffer = []
            for point in pointChunk.points:
                point2d = [point.x, point.y]  # TODO handle 3 dimensions aswell
                pointBuffer.append(point2d)
                wordBuffer.append(point.id)
                clusterBuffer.append(point.clusterID)

            # Provee conntects to this signal, and forwards the points to the visualiser
            self.newPointsSignal.emit(copy.deepcopy(
                pointBuffer), copy.deepcopy(wordBuffer), copy.deepcopy(clusterBuffer))

    def getServiceProgress(self, serviceType, name, serviceProtos, signal, timer):
        """Send a gRPC message to a service to get the progress
        """
        if name not in self.stubs[serviceType]:
            return

        response = self.stubs[serviceType][name].getProgress(
            serviceProtos.google_dot_protobuf_dot_empty__pb2.Empty())
        signal.emit(response.progress)
        # Stop timer if finished
        if response.progress == 100:
            timer.stop()

    def getTransformerProgress(self):
        """Send a gRPC message to transformer to get the progress
        """
        self.getServiceProgress(
            'Transformer', self.activeService["Transformer"], trans, self.TransformerprogressSignal, self.transformerPercentageTimer)

    def getProjectorProgress(self):
        """Send a gRPC message to projector to get the progress
        """
        self.getServiceProgress(
            'Projector', self.activeService["Projector"], proj, self.ProjectorprogressSignal, self.projectorPercentageTimer)

    def getKNNProgress(self):
        """Send a gRPC message to KNN to get the progress
        """
        self.getServiceProgress(
            'KNN', self.activeService["KNN"], knn, self.KNNprogressSignal, self.knnPercentageTimer)

    def getProjectorModel(self):
        """Send a gRPC message to Projector to get the current trained model
        """
        if not self.isServiceActive("Projector"):
            return
        response = self.stubs["Projector"][self.activeService["Projector"]].getModel(
            proj.google_dot_protobuf_dot_empty__pb2.Empty())
        if response.model == pickle.dumps(None):
            return
        self.newProjectorModelSignal.emit(response.model)

        if not self.projectorPercentageTimer.isActive():  # finished
            self.getModelTimer.stop()

    def stopTransformer(self):
        """Stop the projection
        """
        if self.isServiceActive("Transformer"):
            self.stubs["Transformer"][self.activeService["Transformer"]].stopTransformerService(
                trans.google_dot_protobuf_dot_empty__pb2.Empty())

    def makeDensityRequest(self, densityWord):
        if not self.isServiceActive("KNN"):
            return
        print("Density request with word:", densityWord)

        result = self.stubs["KNN"].getAllDistances(
            knn.allDistanceKNNRequest(word=densityWord))
        if len(result.distances) == 0:
            print("error")
            return

        self.densityRequestSignal.emit(result.indices, result.distances)

    def getKNNRequest(self, sentence):
        """Send a gRPC message to KNN to get the result of KNN request

        Args:
            sentence (string): The linalg expression of the request. For example, king - man + woman
        """
        if not self.isServiceActive("KNN"):
            return

        result = self.stubs["KNN"][self.activeService["KNN"]].getKNNRequest(
            knn.knnRequest(k=10, words=sentence))
        if len(result.rows) == 0:  # Something went wrong, check KNN output
            print("Error occurured in KNN process")
            return

        distanceList = []
        wordIndexList = []
        wordList = []
        for row in result.rows:
            distanceList.append(row.distance)
            wordIndexList.append(row.wordIndex)
            wordList.append(row.word)

        self.knnRequestSignal.emit(copy.deepcopy(distanceList), copy.deepcopy(
            wordList), copy.deepcopy(wordIndexList))

    def changeProjector(self, name):
        """Change the active projector and transformer types.

        Args:
            name (string): name of projection method

        Returns:
            bool: returns true if projector type was changed (false if already correct)
        """
        # Return false if not found
        if name not in self.stubs['Projector'] or name not in self.stubs['Transformer']:
            return False
        # Return true if already correct
        if name == self.activeService['Projector']:
            return True

        # Stop and change projector
        if self.activeService['Projector'] is not None:
            self.stubs['Projector'][self.activeService['Projector']].stopProjectorService(
                proj.google_dot_protobuf_dot_empty__pb2.Empty())
        self.activeService['Projector'] = name

        # Stop and change transformer
        if self.activeService['Transformer'] is not None:
            self.stubs['Transformer'][self.activeService['Transformer']].stopTransformerService(
                trans.google_dot_protobuf_dot_empty__pb2.Empty())
        self.activeService['Transformer'] = name
        print('Changed projector: ', self.activeService)
        return True

    def addPointsToValidationKNN(self, points):
        """Add transformed points to the validation kNN to validate precision when later requested.

        Args:
            points ([[float]]): transformed points in 2D/3D
        """
        if not self.stubs['KNN']['validation']:
            return
        self.stubs['KNN']['validation'].addTransformedPoints(
            knn.addTransformedPointsRequest(vectors=[knn.Vector(point=point) for point in points]))

    def clearValidationKNN(self):
        """Clear validation kNN buffer with all transformed points of previous transformation.
        """
        if not self.stubs['KNN']['validation']:
            return
        self.stubs['KNN']['validation'].clearPoints(
            knn.google_dot_protobuf_dot_empty__pb2.Empty())
