import threading
import pickle as pk
import sys
import time
import copy
import os
from concurrent import futures
import grpc
from sklearn.decomposition import IncrementalPCA
import src.generated.Python.projector_pb2_grpc as rpc
import src.generated.Python.projector_pb2 as proj
import numpy as np
from PyQt5 import QtGui
from PyQt5.QtCore import QTimer, pyqtSlot
from sklearn.decomposition import IncrementalPCA
from src.constants import *
from src.Projections.Python.Projector.Projector import Projector
from src.utils.helperFunctions import lineToVector, printColoured


class PCA_Projector(Projector):
    """PCA projector from sklearn

    Args:
        Projector (Projector): General interface for projectors
    """

    def __init__(self):
        super(PCA_Projector, self).__init__()

    def clearVariables(self):
        """Clear all variables
        """
        super(PCA_Projector, self).clearVariables()
        self.ipca = IncrementalPCA(n_components=2, batch_size=16)
        self.fileSize = 0

    def stopProjectorService(self, request, context):
        """Stop the current service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC void
        """
        self.running = False
        printMessage("Stopping service")
        return proj.google_dot_protobuf_dot_empty__pb2.Empty()

    # PCA supports the precision of a model

    def getPrecision(self):
        """Return the precision (Which sklearn PCA provides)

        Returns:
            [type]: [description]
        """
        if not self.ipca:
            return "No model yet"
        return self.ipca.get_precision()

    def getModel(self, request, context):
        """Return the model to gRPC message

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            model: Return the byte model
        """

        # Copying is probably not needed anymore
        modelCopy = copy.deepcopy(self.ipca)
        bytesData = pk.dumps(modelCopy)
        return proj.model(model=bytesData)

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC requset

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return proj.progressProjector(progress=int(self.progress))

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def startProjectorService(self, request, context):
        """Set up the service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC void
        """

        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()

        self.filePath = request.path
        printMessage("Path: {0}".format(self.filePath))
        self.fileSize = os.path.getsize(self.filePath)
        self.running = True
        # Start up separate thread to prevent hanging
        self.t = threading.Thread(target=self.trainPCA)
        self.t.start()
        return proj.google_dot_protobuf_dot_empty__pb2.Empty()

    def trainPCA(self):
        """Starts training the projector
        """
        count = 0
        t1 = time.time()
        printMessage("Starting training!")
        with open(self.filePath, "r", encoding="utf8") as f:

            buffer = []
            for line in f:
                count += 1
                # For keeping track how far we are
                self.sizeSoFar += sys.getsizeof(line)

                if not self.running:  # For stopping the projector
                    printMessage("Training aborted")
                    return

                _, vector = lineToVector(line)
                buffer.append(vector)

                # If the buffer is full, fit it
                # Increases the speed
                if len(buffer) >= TRAINING_BUFFER_SIZE:
                    self.partialFit(buffer)
                    buffer = []

            # Flush the buffer if it is not empty
            if len(buffer) != 0:
                self.partialFit(buffer)

            self.sizeSoFar = self.fileSize
            t2 = time.time()
            printMessage("Finished in {0} seconds".format(t2 - t1))
            self.stop()

    def partialFit(self, buff):
        """Partial fit the buffer

        Args:
            buff ([hdvector]]): The list of the vectors
        """
        self.ipca.partial_fit(buff)


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "Projector PCA", "cyan")


def serveServer():
    """Setup the GRPC server
    """
    port = 'localhost:50052'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_ProjectorServicer_to_server(PCA_Projector(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Projector server")
    serveServer()
