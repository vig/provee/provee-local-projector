import copy
import os
import pickle as pk
import sys
import threading
import time
from concurrent import futures

import grpc
import numpy as np
import src.generated.Python.projector_pb2 as proj
import src.generated.Python.projector_pb2_grpc as rpc
from umap import UMAP, AlignedUMAP
from src.constants import *
from src.Projections.Python.Projector.Projector import Projector
from src.utils.helperFunctions import lineToVector, printColoured
from src.constants import UMAP_BATCH_SIZE, UMAP_OVERLAP
from math import ceil


class UMAPProjector(Projector):
    def __init__(self):
        self.batchSize = UMAP_BATCH_SIZE
        self.overlap = UMAP_OVERLAP
        super(UMAPProjector, self).__init__()

    def clearVariables(self):
        """Clear all variables
        """
        super(UMAPProjector, self).clearVariables()
        self.umapModel = None
        self.fileSize = 0
        self.filePath = None
        self.t = None
        self.bufferFileSize = 0
        self.sharedPoints = ceil(self.overlap * self.batchSize)
        self.newPointsPerBatch = self.batchSize - self.sharedPoints

    def train(self):
        count = 0
        batchCount = 0
        t1 = time.time()
        relationDict = {
            i + self.newPointsPerBatch: i for i in range(self.sharedPoints)}
        printMessage("Starting training UMAP")

        # Fitting
        with open(self.filePath, "r", encoding="utf8") as f:
            pointBuffer = []
            labelBuffer = []
            self.bufferFileSize = 0
            for line in f:
                label, vector = lineToVector(line)
                pointBuffer.append(vector)
                labelBuffer.append(label)
                count += 1
                self.bufferFileSize += sys.getsizeof(line)

                if not self.running:
                    printMessage("Training aborted")
                    return

                if self.umapModel is None:
                    # First batch has no overlap (self.batchSize), second batch has overlapping points in first batch so only needs new points (self.newPointsPerBatch)
                    if count >= self.batchSize + self.newPointsPerBatch:
                        batchCount = 2
                        printMessage('First two batches')
                        self.umapModel = AlignedUMAP().fit([np.array(pointBuffer[:self.batchSize]), np.array(
                            pointBuffer[self.newPointsPerBatch:])], relations=[relationDict.copy()])
                        pointBuffer, count = self.clearPointBufferExceptShared(
                            pointBuffer)
                else:
                    # Clear batch when enough new points have been added to the shared points that remained in the buffer
                    if count >= self.batchSize:
                        batchCount += 1
                        printMessage('New batch: {0}'.format(batchCount))
                        self.umapModel.update(np.array(pointBuffer),
                                              relations=relationDict.copy())
                        pointBuffer, count = self.clearPointBufferExceptShared(
                            pointBuffer)

            printMessage('Final batch: {0}'.format(len(pointBuffer)))
            if len(pointBuffer) > 0:
                if self.umapModel is None:
                    # If batch size is large enough fit all points in the batch and train with normal UMAP
                    printMessage('Single UMAP batch')
                    self.umapModel = UMAP().fit(np.array(pointBuffer))
                else:
                    batchCount += 1
                    printMessage('New batch: {0}'.format(batchCount))
                    self.umapModel.update(np.array(pointBuffer),
                                          relations=relationDict.copy())
                self.sizeSoFar += self.bufferFileSize
                self.bufferFileSize = 0

        t2 = time.time()
        printMessage("Finished in {0} seconds".format(t2 - t1))
        self.stop()

    def clearPointBufferExceptShared(self, pointBuffer):
        # Store shared points for next batch
        pointBuffer = pointBuffer[len(pointBuffer) -
                                  self.sharedPoints:]
        count = self.sharedPoints
        self.sizeSoFar += self.bufferFileSize
        self.bufferFileSize = 0

        return pointBuffer, count

    def getPrecision(self):
        return "No supported UMAP precision"

    def formatAndCopyModel(self):
        modelCopy = None
        if isinstance(self.umapModel, UMAP):
            modelCopy = self.umapModel
        elif isinstance(self.umapModel, AlignedUMAP):
            modelCopy = []
            for mapper in self.umapModel.mappers_:
                modelCopy.append(copy.deepcopy(mapper))
        return modelCopy

    def getModel(self, request, context):
        """Return the model to gRPC message

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            model: Return the byte model
        """
        # Copying is probably not needed anymore
        modelCopy = self.formatAndCopyModel()
        bytesData = pk.dumps(modelCopy)
        return proj.model(model=bytesData)

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC requset

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return proj.progressProjector(progress=int(self.progress))

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar or self.fileSize is None or self.fileSize == 0:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def start(self, path):
        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()

        self.filePath = path
        printMessage("Path: {0}".format(self.filePath))
        self.fileSize = os.path.getsize(self.filePath)
        self.running = True

        self.train()

    def startProjectorService(self, request, context):
        """Set up the service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            gRPC void
        """
        # Start up separate thread to prevent hanging
        self.t = threading.Thread(target=self.start, kwargs={"path": request.path})
        self.t.start()
        return proj.google_dot_protobuf_dot_empty__pb2.Empty()

    def stopProjectorService(self, request, context):
        """Stop the current service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC void
        """
        self.stop()
        printMessage("Stopping service")
        return proj.google_dot_protobuf_dot_empty__pb2.Empty()


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it
    Args:
        message (string): The actual message
    """
    printColoured(message, "Projector UMAP", "cyan")


def serveServer():
    """Setup the GRPC server
    """
    port = 'localhost:50054'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_ProjectorServicer_to_server(UMAPProjector(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Projector server")
    serveServer()
