from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot


# Interface for the Projectors
class Projector(QThread):
    """Projector interface from which all Python projectors should inherit (WIP)

    Args:
        QThread (QThread): So that we can use the QT signals / slots (Maybe change this QThread to Qobject)
    """
    progressSignal = pyqtSignal(int)

    def __init__(self):
        super(Projector, self).__init__()
        self.clearVariables()

    def clearVariables(self):
        """Clear all variables
        """
        self.running = False
        self.sizeSoFar = 0
        self.progress = 0

    # This is the slot that the thread will call.
    @pyqtSlot()
    def run(self):
        raise NotImplementedError

    def stop(self):
        self.running = False

    def getModel(self):
        raise NotImplementedError

    def train(self):
        raise NotImplementedError
