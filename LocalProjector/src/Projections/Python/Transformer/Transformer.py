from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot


class Transformer(QThread):
    """Transformer interface from which all Python transformers should inherit (WIP)

    Args:
        QThread (QThread): So that we can use the QT signals / slots (Maybe change this QThread to Qobject)
    """
    # Signal for emitting new points, whilst being thread-safe
    newPointsSignal = pyqtSignal(object)
    progressSignal = pyqtSignal(int)

    def __init__(self):
        super(Transformer, self).__init__()
        self.running = True
        self.progress = 0

    # This is the slot that the thread will call.
    @pyqtSlot()
    def run(self):
        raise NotImplementedError

    def stop(self):
        self.running = False

    def transform(self):
        raise NotImplementedError
