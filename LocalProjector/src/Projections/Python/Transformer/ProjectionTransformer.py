import os
import pickle as pk
import sys
import threading
import time
from concurrent import futures

import grpc
import src.generated.Python.transformer_pb2 as trans
import src.generated.Python.transformer_pb2_grpc as rpc
from PyQt5 import QtGui
from PyQt5.QtCore import (QObject, QRunnable, QThread, QTimer, pyqtSignal,
                          pyqtSlot)
from src.constants import *
import src.Projections.Python.Transformer.Transformer as t
from src.utils.helperFunctions import lineToVector, printColoured


class ProjectionTransformer(t.Transformer):
    """The Transformer

    Args:
        t (Transformer): General interface for transformers
    """

    def __init__(self):
        super(ProjectionTransformer, self).__init__()
        self.clearVariables()
        self.bufferSize = TRANSFORMING_BUFFER_SIZE

    def clearVariables(self):
        """Clear all variables
        """
        self.sizeSoFar = None
        self.model = None
        self.filePath = None
        self.fileSize = None
        self.running = False
        self.maxCount = -1
        self.isClusterFile = False

    def checkIfAbort(self, count):
        """Check if the transformer should be aborter

        Args:
            count (int): How many points have been processed

        Returns:
            bool: Whether abort or not
        """
        if not self.running:
            printMessage("Transforming aborted")
            return True

        if count >= self.maxCount and self.maxCount > 0:
            printMessage(
                "Finished transforming of {0} points".format(self.maxCount))
            return True

        return False

    def stopTransformerService(self, request, context):
        """Stop the current service

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC void
        """
        self.running = False
        printMessage("Stopping service")
        return trans.google_dot_protobuf_dot_empty__pb2.Empty()

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar or not self.fileSize:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC requset

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return trans.progressTransformer(progress=int(self.progress))

    def transformAndToGRPC(self, buffer):
        """Transform and create the gRPC messages

        Args:
            buffer ([hdvector]): The list of the actual word vectors in

        Returns:
           pointChunk: list of points to be returned to main applications
        """
        words = [el[0] for el in buffer]
        vectors = [el[1] for el in buffer]
        results = self.model.transform(vectors)
        pointList = []
        clusterIDs = words if self.isClusterFile else [0] * len(words)
        for i in range(len(results)):
            pointList.append(trans.point(
                id=words[i], x=results[i][0], y=results[i][1], clusterID=int(clusterIDs[i])))
        return trans.pointChunk(points=pointList)

    def startTransformerService(self, request, context):
        """Starts projecting the points

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Yields:
            pointChunk: gRPC messages containing the list of projected points
        """
        printMessage("Starting transforming")
        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()
        self.running = True
        # Transform bytes object to python object
        self.model = pk.loads(request.model)
        self.filePath = request.path
        self.fileSize = os.path.getsize(self.filePath)

        self.maxCount = -1

        count = 0
        self.sizeSoFar = 0
        t1 = time.time()

        extension = self.filePath.split(".")[-1]
        self.isClusterFile = (extension == "ctxt")

        with open(self.filePath, "r", encoding="utf8") as f:
            buffer = []
            for line in f:
                # For keeping track how far we are
                self.sizeSoFar += sys.getsizeof(line)
                count += 1

                if self.checkIfAbort(count):
                    # So that the main program knows it is finished
                    yield trans.pointChunk(points=[])
                    return

                # If user wants to specify for example transform 10.000 instead of everything
                word, vector = lineToVector(line)
                buffer.append((word, vector))
                # If the buffer is full, flush it
                if len(buffer) >= self.bufferSize:
                    pointChunk = self.transformAndToGRPC(buffer)
                    if pointChunk is None:
                        break
                    buffer = []
                    yield pointChunk

            # Flush the remaining buffer
            if len(buffer) > 0:
                yield self.transformAndToGRPC(buffer)

            # Send empty so that the main program knows it is done
            yield trans.pointChunk(points=[])
            # finished
            self.progress = 100
            self.sizeSoFar = self.fileSize

        t2 = time.time()
        printMessage("Finished in {0}".format(t2 - t1))


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "Transformer Base", "yellow")


def serveServer():
    """Setup the GRPC server
    """
    port = 'localhost:50053'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_TransformerServicer_to_server(ProjectionTransformer(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Transformer server")
    serveServer()
