import os
import pickle as pk
import sys
import threading
import time
from concurrent import futures

import grpc
import src.generated.Python.transformer_pb2 as trans
import src.generated.Python.transformer_pb2_grpc as rpc
from PyQt5 import QtGui
from PyQt5.QtCore import (QObject, QRunnable, QThread, QTimer, pyqtSignal,
                          pyqtSlot)
from src.constants import *
import src.Projections.Python.Transformer.Transformer as t
from src.utils.helperFunctions import lineToVector, printColoured
from src.Projections.Python.Transformer.ProjectionTransformer import ProjectionTransformer
from scipy.linalg import orthogonal_procrustes
import numpy as np
from math import ceil


class UMAPTransformer(ProjectionTransformer):
    """The Transformer

    Args:
        t (Transformer): General interface for transformers
    """

    def __init__(self):
        self.batchSize = UMAP_BATCH_SIZE
        self.overlap = UMAP_OVERLAP
        self.scaling = 10
        super(UMAPTransformer, self).__init__()

    def clearVariables(self):
        """Clear all variables
        """
        super(UMAPTransformer, self).clearVariables()
        self.prevTransformedOverlap = None
        self.pointIndex = 0
        self.mapperIndex = 0
        self.sharedPoints = ceil(self.overlap * self.batchSize)
        self.newPointsPerBatch = self.batchSize - self.sharedPoints
        self.bufferSize = UMAP_BATCH_SIZE
        self.sharedPointsBuffer = None

    def transformAndToGRPC(self, pointBuffer):
        """Transform and create the gRPC messages

        Args:
            pointBuffer ([hdvector]): The list of the actual word vectors in

        Returns:
           pointChunk: list of points to be returned to main applications
        """
        labelBuffer = np.array([el[0] for el in pointBuffer])
        pointBuffer = np.array([el[1] for el in pointBuffer])
        newPoints = len(pointBuffer)
        pointList = []

        clusterIDs = None
        if self.isClusterFile:
            clusterIDs = labelBuffer
        if isinstance(self.model, list):
            if self.sharedPointsBuffer is not None:
                pointBuffer = np.concatenate(
                    (self.sharedPointsBuffer, pointBuffer))

            if self.mapperIndex >= len(self.model):
                printMessage('All mappers have been fully used. Stopped transforming at {0}.'.format(
                    (self.progress)))
                return None

            if clusterIDs is None:
                clusterIDs = np.repeat(self.mapperIndex, len(labelBuffer))

            printMessage('Transform index {0} mapper {1}'.format(
                self.pointIndex, self.mapperIndex))

            transformed = self.model[self.mapperIndex].transform(pointBuffer)
            transformed, self.prevTransformedOverlap = self.alignTransformedSets(
                transformed, self.prevTransformedOverlap)

            for n, i in enumerate(range(self.sharedPoints if self.mapperIndex != 0 else 0, len(transformed))):
                pointList.append(trans.point(
                    id=labelBuffer[n], x=transformed[i, 0], y=transformed[i, 1], clusterID=int(clusterIDs[n])))

            if self.pointIndex == 0:
                self.bufferSize = self.newPointsPerBatch
            self.sharedPointsBuffer = pointBuffer[len(
                pointBuffer) - self.sharedPoints:]

            self.mapperIndex += 1
        else:
            if clusterIDs is None:
                clusterIDs = np.zeros(len(labelBuffer))

            transformed = self.model.transform(pointBuffer)

            for i in range(len(transformed)):
                pointList.append(trans.point(
                    id=labelBuffer[i], x=transformed[i, 0], y=transformed[i, 1], clusterID=int(clusterIDs[i])))
        self.pointIndex += newPoints
        return trans.pointChunk(points=pointList)

    def alignTransformedSets(self, transformed, prevTransformedSharedPoints):
        def getSharedWithPrevious(transfromedPoints):
            return transfromedPoints[:self.sharedPoints]

        def getSharedWithNextBatch(transfromedPoints):
            return transfromedPoints[len(transfromedPoints) - self.sharedPoints:]

        if prevTransformedSharedPoints is None:
            transformed = self.alignWithOrigin(
                transformed, getSharedWithNextBatch)
        else:
            transformed = self.alignWithOrigin(
                transformed, getSharedWithPrevious)

            # print('Before disparity: ', np.sum(
            #    np.square(prevTransformedSharedPoints - getSharedWithPrevious(transformed))))
            # Rotate shared points to match previous batch
            procrustesMatrix, _ = orthogonal_procrustes(
                getSharedWithPrevious(transformed), prevTransformedSharedPoints)
            transformed = np.dot(
                transformed, procrustesMatrix.T)
            # print('After disparity: ', np.sum(
            #    np.square(prevTransformedSharedPoints - getSharedWithPrevious(transformed))))

        transformed *= self.scaling
        return transformed, getSharedWithNextBatch(transformed)

    def alignWithOrigin(self, transformed, getSharedPoints):
        if len(getSharedPoints(transformed)) == 0:
            return transformed

        # Place overlapping points with previous batch in origin
        transformed -= np.mean(getSharedPoints(transformed), 0)
        # Normalize scaling of overlap
        norm = np.linalg.norm(getSharedPoints(transformed))
        transformed /= norm
        return transformed


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "Transformer UMAP", "yellow")


def serveServer():
    """Setup the GRPC server
    """
    port = 'localhost:50055'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_TransformerServicer_to_server(UMAPTransformer(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Transformer server")
    serveServer()
