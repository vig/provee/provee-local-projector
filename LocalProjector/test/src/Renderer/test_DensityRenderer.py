from src.Renderers.DensityRenderer import *
import pytest
import numpy as np
import os
from unittest.mock import call, Mock, patch, MagicMock, mock_open
from src.UI.rangeslider import QRangeSlider

dir_path = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture
def baseRenderer(mocker):
    """Returns a clean DensityRenderer with mocked render functions."""
    setupMock = mocker.patch(
        'src.Renderers.DensityRenderer.DensityRenderer.setupUI')
    updateMock = mocker.patch(
        'src.Renderers.DensityRenderer.DensityRenderer.update')

    densityRenderer = DensityRenderer(0)
    densityRenderer.setupMock = setupMock
    densityRenderer.updateMock = updateMock

    return densityRenderer


class TestRenderer:
    def test_init(self, baseRenderer):
        assert baseRenderer.kdTree is None

        assert baseRenderer.kdTree is None
        assert baseRenderer.clickedPoint is None
        assert baseRenderer.wordTrie is None

        assert baseRenderer.densityRequestIndices is None
        assert baseRenderer.densityRequestDistances is None

        assert len(baseRenderer.clusterIDs) is 0
        assert len(baseRenderer.data) is 0

        # Check mocks created in baseRenderer() fixture
        baseRenderer.setupMock.assert_called_once()
        baseRenderer.updateMock.assert_called_once()

    def test_removeAllPoints(self, baseRenderer):
        assert len(baseRenderer.clusterIDs) == 0
        assert len(baseRenderer.data) == 0

        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")
        baseRenderer.removeAllPoints()

        assert len(baseRenderer.clusterIDs) == 0
        assert len(baseRenderer.data) == 0

    def test_findWordWithData(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")
        wordList, _ = baseRenderer.findWord("king")

        assert len(wordList) == 3

    def test_generateTrees(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        assert baseRenderer.wordTrie is not None
        assert baseRenderer.kdTree is not None

        assert len(baseRenderer.kdTree.get_arrays()[0]) == 9000
        assert len(baseRenderer.wordTrie.items()) == 9000

    def test_getColoursSize(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        colours = baseRenderer.getColours()
        assert len(colours) == 9000

        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset5k.txt")

        colours = baseRenderer.getColours()
        assert len(colours) == 5000

    def test_getSizesSize(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        colours = baseRenderer.getSizes()
        assert len(colours) == 9000

        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset5k.txt")

        colours = baseRenderer.getSizes()
        assert len(colours) == 5000

    def test_getDensityShape(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        # Mock canvas
        baseRenderer.canvas = Mock()
        baseRenderer.canvas.size = (600, 800)
        # Mock camera
        baseRenderer.view = Mock()
        baseRenderer.view.camera.rect.bottom = 0
        baseRenderer.view.camera.rect.top = 1
        baseRenderer.view.camera.rect.left = 0
        baseRenderer.view.camera.rect.right = 1
        # Block signal
        signalNewMaxDensity = Mock()
        baseRenderer.newMaxDensity = signalNewMaxDensity

        array = baseRenderer.getDensity(baseRenderer.data)
        nx, ny = baseRenderer.canvas.size
        bins = (nx // baseRenderer.densityBinFactor,
                ny // baseRenderer.densityBinFactor)
        assert array.shape == bins
        signalNewMaxDensity.emit.assert_called_once_with(int(np.max(array)))

    def test_getDensityValues(self, baseRenderer):
        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        # Mock canvas
        baseRenderer.canvas = Mock()
        baseRenderer.canvas.size = (600, 800)
        # Mock camera
        baseRenderer.view = Mock()
        baseRenderer.view.camera = Mock()
        baseRenderer.view.camera.rect = Mock()
        baseRenderer.view.camera.rect.bottom = 1
        baseRenderer.view.camera.rect.top = 2
        baseRenderer.view.camera.rect.left = 1
        baseRenderer.view.camera.rect.right = 2
        # Block signal
        signalNewMaxDensity = Mock()
        baseRenderer.newMaxDensity = signalNewMaxDensity

        array = baseRenderer.getDensity(baseRenderer.data)
        assert (array >= 0).all()
        signalNewMaxDensity.emit.assert_called_once_with(int(np.max(array)))

    def test_findWordWithoutData(self, baseRenderer):
        wordList, _ = baseRenderer.findWord("king")
        assert len(wordList) == 0

    def test_upload(self, baseRenderer):

        baseRenderer.uploadProjectionData(
            dir_path + "/testData/dataset10k.txt")

        assert len(baseRenderer.clusterIDs) == 9000
        assert len(baseRenderer.data) == 9000

        baseRenderer.uploadProjectionData(dir_path + "/testData/dataset5k.txt")

        assert len(baseRenderer.clusterIDs) == 5000
        assert len(baseRenderer.data) == 5000

        baseRenderer.removeAllPoints()

        assert len(baseRenderer.clusterIDs) == 0
        assert len(baseRenderer.data) == 0
