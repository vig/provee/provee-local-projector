import pytest
from src.constants import *


class TestConstants:
    def test_all_constants(self):
        assert isinstance(TRAINING_BUFFER_SIZE, int)
        assert isinstance(TRANSFORMING_BUFFER_SIZE, int)
        assert isinstance(UMAP_BATCH_SIZE, int)
        assert isinstance(UMAP_OVERLAP, float)
        assert isinstance(VALIDATIONKNN_BATCH_SIZE, int)
        assert isinstance(VALIDATIONKNN_NNEIGBOURS, list)
        for x in VALIDATIONKNN_NNEIGBOURS:
            assert isinstance(x, int)
            assert x > 0
        assert isinstance(SLEEP_COUNT, int)
        assert isinstance(SLEEP_AMOUNT, int)
