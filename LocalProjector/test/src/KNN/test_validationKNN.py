import pytest
from unittest.mock import call, Mock, patch, MagicMock, mock_open
from src.constants import *
from src.KNN.validationKNN import *
import numpy as np
from time import sleep


SCRIPT_IMPORT_PATH = 'src.KNN.validationKNN'
FILE_SIZE = 6
FILE_PATH = 'test\\file\\mock'


@pytest.fixture
def validationKNN(mocker):
    """Returns a clean ValidationKNN instance."""
    return ValidationKNN()


class TestValidationKNN:
    def test_init(self, mocker, validationKNN):
        assert validationKNN.vectorBatchSize == VALIDATIONKNN_BATCH_SIZE
        assert validationKNN.nNeighbours == VALIDATIONKNN_NNEIGBOURS
        assert validationKNN.originalKNNStub is None

        assert validationKNN.transformedPoints == []
        assert not validationKNN.updated

        assert not validationKNN.running

    def test_clearVariables(self, mocker, validationKNN):
        validationKNN.clearVariables()

        assert validationKNN.index is None
        assert validationKNN.sizeSoFar == 0
        assert validationKNN.progress == 0
        assert validationKNN.fileSize == 0
        assert validationKNN.filePath is None
        assert validationKNN.nearestNeighboursModel is None
        assert validationKNN.correctKNNs == [0] * len(VALIDATIONKNN_NNEIGBOURS)
        assert validationKNN.totalKNNs == [0] * len(VALIDATIONKNN_NNEIGBOURS)
        assert not validationKNN.running

        validationKNN.nNeighbours = [1, 2, 3, 4, 5, 6]
        validationKNN.clearVariables()
        assert validationKNN.correctKNNs == [
            0] * len(validationKNN.nNeighbours)
        assert validationKNN.totalKNNs == [0] * len(validationKNN.nNeighbours)

    def test_addTransformedPoints(self, mocker, validationKNN):
        points = [[1, 2], [3, 4]]
        request = knn.addTransformedPointsRequest(
            vectors=[knn.Vector(point=point) for point in points])
        result = validationKNN.addTransformedPoints(request, None)

        assert not validationKNN.updated
        assert validationKNN.transformedPoints == points
        assert result == knn.google_dot_protobuf_dot_empty__pb2.Empty()

        # Check if updated is set to false and if old transformedPoints are not overwritten
        validationKNN.updated = True
        newPoints = [[5, 6], [7, 8]]
        request = knn.addTransformedPointsRequest(
            vectors=[knn.Vector(point=point) for point in newPoints])
        result = validationKNN.addTransformedPoints(request, None)

        assert not validationKNN.updated
        assert validationKNN.transformedPoints == points + newPoints
        assert result == knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def test_clearPoints(self, mocker, validationKNN):
        validationKNN.transformedPoints = [[1, 2], [3, 4]]
        validationKNN.updated = True
        request = knn.google_dot_protobuf_dot_empty__pb2.Empty()
        result = validationKNN.clearPoints(request, None)

        assert validationKNN.transformedPoints == []
        assert not validationKNN.updated
        assert result == knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def test_updateKNN(self, mocker, validationKNN):
        modelMock = Mock()
        modelMock.fit = Mock(return_value='modelMock')
        mocker.patch(SCRIPT_IMPORT_PATH +
                     '.sklearn.neighbors.NearestNeighbors', return_value=modelMock)
        points = [[1, 2], [3, 4]]
        validationKNN.transformedPoints = points

        pointsUsed = validationKNN.updateKNN()

        assert validationKNN.updated
        assert validationKNN.nearestNeighboursModel == 'modelMock'
        assert (points == pointsUsed).all()
        modelMock.fit.assert_called_once_with(pointsUsed)

        # Check if mock is not called when updated and has a model
        pointsUsed = validationKNN.updateKNN()

        assert validationKNN.updated
        assert validationKNN.nearestNeighboursModel == 'modelMock'
        assert (points == pointsUsed).all()
        modelMock.fit.assert_called_once()

        # Check if method is called when updated, but there is no model
        validationKNN.nearestNeighboursModel = None
        pointsUsed = validationKNN.updateKNN()

        assert validationKNN.updated
        assert validationKNN.nearestNeighboursModel == 'modelMock'
        assert (points == pointsUsed).all()
        assert len(modelMock.fit.mock_calls) == 2
        modelMock.fit.assert_called_with(pointsUsed)

        # Check if method is called when not updated
        validationKNN.updated = False
        pointsUsed = validationKNN.updateKNN()

        assert validationKNN.updated
        assert validationKNN.nearestNeighboursModel == 'modelMock'
        assert (points == pointsUsed).all()
        assert len(modelMock.fit.mock_calls) == 3
        modelMock.fit.assert_called_with(pointsUsed)

    def test_getProgress(self, mocker, validationKNN):
        validationKNN.sizeSoFar = 3
        validationKNN.fileSize = FILE_SIZE
        progress = int((3 / FILE_SIZE * 100))

        result = validationKNN.getProgress(None, None)

        assert result == knn.progressKNN(progress=int(progress))

    def test_updatePercentage(self, mocker, validationKNN):
        # Test default
        validationKNN.updatePercentage()
        assert validationKNN.progress == 0
        # Test with no fileSize
        validationKNN.sizeSoFar = 3
        validationKNN.updatePercentage()
        assert validationKNN.progress == 0
        # Test with fileSize
        validationKNN.fileSize = FILE_SIZE
        validationKNN.updatePercentage()
        assert validationKNN.progress == (3 / FILE_SIZE * 100)
        # Test bigger than fileSize
        validationKNN.sizeSoFar = FILE_SIZE * 2
        validationKNN.updatePercentage()
        assert validationKNN.progress == 100

    def test_compareKNNs(self, mocker, validationKNN):
        def correctAndTotal(originalVectors, transformedVectors, k):
            return (2 * k, 3 * k)

        nNeighbours = [3, 4, 10, 1]
        validationKNN.nNeighbours = nNeighbours
        validationKNN.clearVariables()

        compareBatchMock = mocker.patch(SCRIPT_IMPORT_PATH + '.ValidationKNN.compareKNNBatch',
                                        side_effect=correctAndTotal)

        originalVectors = [[1, 1, 1], [2, 2, 2]]
        transformedVectors = [[1, 1], [2, 2]]
        result = validationKNN.compareKNNs(
            originalVectors, transformedVectors)

        for i, k in enumerate(nNeighbours):
            (correct, total) = correctAndTotal(
                originalVectors, transformedVectors, k)
            assert validationKNN.correctKNNs[i] == correct
            assert validationKNN.totalKNNs[i] == total

        # Check if calls are summed
        result = validationKNN.compareKNNs(
            originalVectors, transformedVectors)

        for i, k in enumerate(nNeighbours):
            (correct, total) = correctAndTotal(
                originalVectors, transformedVectors, k)
            assert validationKNN.correctKNNs[i] == correct * 2
            assert validationKNN.totalKNNs[i] == total * 2

    def test_compareKNNBatch(self, mocker, validationKNN):
        nNeighbours = 3
        originalNNsMock = mocker.patch(SCRIPT_IMPORT_PATH + '.ValidationKNN.getOriginalNNBatch',
                                       return_value=np.array([[1, 2, 3], [4, 5, 6]]))
        modelMock = Mock()
        modelMock.kneighbors = Mock(return_value=(
            ['distances that are not used'], [[1, 5, 3], [6, 7, 8]]))
        validationKNN.nearestNeighboursModel = modelMock

        originalVectors = [[1, 1, 1], [2, 2, 2]]
        transformedVectors = [[1, 1], [2, 2]]
        result = validationKNN.compareKNNBatch(
            originalVectors, transformedVectors, nNeighbours)

        assert result == (3, 6)
        originalNNsMock.assert_called_once_with(originalVectors, nNeighbours)
        modelMock.kneighbors.assert_called_once_with(
            X=transformedVectors, n_neighbors=nNeighbours)

        # Assert invalid nNeighbours exception
        assert validationKNN.compareKNNBatch(
            originalVectors, transformedVectors, 0) == (None, None)
        assert validationKNN.compareKNNBatch(
            originalVectors, transformedVectors, -10) == (None, None)

        # Assert no originalNNs response exception
        originalNNsMock = mocker.patch(SCRIPT_IMPORT_PATH + '.ValidationKNN.getOriginalNNBatch',
                                       return_value=None)
        assert validationKNN.compareKNNBatch(
            originalVectors, transformedVectors, nNeighbours) == (None, None)

        # Assert different shape NNs
        originalNNsMock = mocker.patch(SCRIPT_IMPORT_PATH + '.ValidationKNN.getOriginalNNBatch',
                                       return_value=[[1, 2, 3]])
        assert validationKNN.compareKNNBatch(
            originalVectors, transformedVectors, nNeighbours) == (None, None)

    def test_getOriginalNNBatch(self, mocker, validationKNN):
        nNeighbours = 2
        originalKNNMock = Mock()
        # NeighboursBatch = [[1, 2], [3, 4]]
        neighboursBatch = knn.NeighboursBatch(neighbours=[
            knn.Neighbours(rows=[knn.Row(wordIndex=1, word="hi", distance=1), knn.Row(
                wordIndex=2, word="hey", distance=2)]),
            knn.Neighbours(rows=[knn.Row(wordIndex=3, word="hi", distance=3), knn.Row(
                wordIndex=4, word="hey", distance=4)])
        ])
        originalKNNMock.getKNNBatch = Mock(return_value=neighboursBatch)
        expected = [[1, 2], [3, 4]]

        vectors = [[1, 1], [2, 2]]
        vectorBatchRequest = knn.knnBatchRequest(vectors=[knn.Vector(
            point=vector) for vector in vectors], k=nNeighbours)

        # Assert that None is returned without a working connection
        assert validationKNN.getOriginalNNBatch(vectors, nNeighbours) is None

        # Test with Mock connection
        validationKNN.originalKNNStub = originalKNNMock
        result = validationKNN.getOriginalNNBatch(vectors, nNeighbours)

        assert (result == expected).all()
        originalKNNMock.getKNNBatch.assert_called_once_with(vectorBatchRequest)

    def test_calcAccuracy(self, mocker, validationKNN):
        nNeighbours = [3, 4, 10, 1]
        validationKNN.nNeighbours = nNeighbours
        validationKNN.correctKNNs = [1, 2, 3, 4]
        validationKNN.totalKNNs = [10, 10, 0, -1]
        expected = [1 / 10 * 100, 2 / 10 * 100, 0, 0]

        accuracyDict = validationKNN.calcAccuracy()

        assert len(accuracyDict) == len(nNeighbours)
        for i, (_, accuracy) in enumerate(accuracyDict.items()):
            assert accuracy == expected[i]

    def test_getAccuracy(self, mocker, validationKNN):
        accuracyDict = {1: 20.1, 3: 0}
        accuracyMock = mocker.patch(SCRIPT_IMPORT_PATH + '.ValidationKNN.calcAccuracy',
                                    return_value=accuracyDict)
        result = validationKNN.getAccuracy(None, None)
        assert result == knn.accuracyKNN(nNeighbours=list(
            accuracyDict.keys()), accuracy=list(accuracyDict.values()))

    def test_validateProjection(self, mocker, validationKNN):
        mocker.patch('sys.getsizeof', return_value=1)
        mocker.patch('os.path.getsize', return_value=FILE_SIZE)
        transformed = [[1.1, 1.2], [2.1, 2.2], [
            3.1, 3.2], [4.1, 4.2], [5.1, 5.2], [6.1, 6.2]]
        connectMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.ValidationKNN.connectToOriginalKNN', return_value=False)
        updateMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.ValidationKNN.updateKNN', return_value=transformed)
        compareMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.ValidationKNN.compareKNNs', return_value=False)
        accuracy = {3: 10.0, 1: 5.0}
        accuracyMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.ValidationKNN.calcAccuracy', return_value=accuracy)

        validationKNN.vectorBatchSize = 4
        expectedCompareCalls = [[[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [1.0, 2.0, 3.0], [4.0, 5.0, 6.0]], [
            [1.1, 1.2], [2.1, 2.2], [3.1, 3.2], [4.1, 4.2]], [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]], [[5.1, 5.2], [6.1, 6.2]]]

        filedata = 'firstLine 1.0 2.0 3.0\nsecondLine 4.0 5.0 6.0\n' + \
            'thirdLine 1.0 2.0 3.0\nfourthLine 4.0 5.0 6.0\n' + \
            'fifthLine 1.0 2.0 3.0\nsixthLine 4.0 5.0 6.0\n'

        open_mock = mock_open(read_data=filedata)
        with patch('builtins.open', open_mock) as mock_file:
            assert open("path\\doensn't\\matter").read() == filedata

            # Test cancel when no filePath was given
            result = validationKNN.validateProjection()
            assert result is None
            connectMock.assert_not_called()
            updateMock.assert_not_called()
            compareMock.assert_not_called()
            accuracyMock.assert_not_called()

            validationKNN.filePath = FILE_PATH
            validationKNN.fileSize = FILE_SIZE

            # Test cancel when disconnected to originalKNN
            result = validationKNN.validateProjection()
            assert result is None
            connectMock.assert_called_once()
            updateMock.assert_not_called()
            compareMock.assert_not_called()
            accuracyMock.assert_not_called()

            connectMock.return_value = True

            # Test cancel when no transformed points have arrived
            result = validationKNN.validateProjection()
            assert result is None
            assert len(connectMock.mock_calls) == 2
            updateMock.assert_not_called()
            compareMock.assert_not_called()
            accuracyMock.assert_not_called()

            validationKNN.transformedPoints = transformed

            # Test cancel when not running
            result = validationKNN.validateProjection()
            assert result is None
            assert len(connectMock.mock_calls) == 3
            updateMock.assert_called_once()
            compareMock.assert_not_called()
            accuracyMock.assert_not_called()

            validationKNN.running = True

            # Test cancel on unsuccesful compareKNN
            result = validationKNN.validateProjection()
            assert result is None
            assert len(connectMock.mock_calls) == 4
            assert len(updateMock.mock_calls) == 2
            compareMock.assert_called_once_with(
                expectedCompareCalls[0], expectedCompareCalls[1])
            accuracyMock.assert_not_called()

            compareMock.return_value = True

            # Test no cancel, normal call
            result = validationKNN.validateProjection()
            assert result == accuracy
            assert len(connectMock.mock_calls) == 5
            assert len(updateMock.mock_calls) == 3
            assert compareMock.mock_calls == [
                # call of previous test
                call(expectedCompareCalls[0], expectedCompareCalls[1]),
                # new calls
                call(expectedCompareCalls[0], expectedCompareCalls[1]),
                call(expectedCompareCalls[2], expectedCompareCalls[3])]
            accuracyMock.assert_called_once()

            validationKNN.clearVariables()
            validationKNN.running = True
            validationKNN.filePath = FILE_PATH
            validationKNN.fileSize = FILE_SIZE

            # Test with unequal amount of points in file and transformed
            transformed = [[1.1, 1.2], [2.1, 2.2]]
            updateMock.return_value = transformed
            result = validationKNN.validateProjection()
            assert result == accuracy
            assert len(connectMock.mock_calls) == 6
            assert len(updateMock.mock_calls) == 4
            assert compareMock.mock_calls == [
                # call of previous test
                call(expectedCompareCalls[0], expectedCompareCalls[1]),
                call(expectedCompareCalls[0], expectedCompareCalls[1]),
                call(expectedCompareCalls[2], expectedCompareCalls[3]),
                # new calls
                call(expectedCompareCalls[0][0:2], transformed)]
            assert len(accuracyMock.mock_calls) == 2

    def test_startKNNService(self, mocker, validationKNN):
        validationMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.ValidationKNN.validateProjection')
        mockGetSize = mocker.patch('os.path.getsize', return_value=FILE_SIZE)

        request = knn.startKNN(path=FILE_PATH)
        validationKNN.sizeSoFar = 1000

        result = validationKNN.startKNNService(request, None)

        assert result == knn.google_dot_protobuf_dot_empty__pb2.Empty()
        assert validationKNN.sizeSoFar == 0
        assert validationKNN.filePath == FILE_PATH
        assert validationKNN.fileSize == FILE_SIZE
        assert validationKNN.running
        # Give thread time to call mock
        sleep(1)
        validationMock.assert_called_once

    def test_stopKNNService(self, mocker, validationKNN):
        validationKNN.running = True
        result = validationKNN.stopKNNService(None, None)
        assert not validationKNN.running
        assert result == knn.google_dot_protobuf_dot_empty__pb2.Empty()

    def test_connectToOriginalKNN(self):
        # TODO
        return
