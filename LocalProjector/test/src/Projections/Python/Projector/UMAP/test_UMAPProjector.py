import pytest
from src.Projections.Python.Projector.UMAP.UMAPProjector import *
from src.constants import *
from unittest.mock import call, Mock, patch, MagicMock, mock_open


SCRIPT_IMPORT_PATH = 'src.Projections.Python.Projector.UMAP.UMAPProjector'
FILE_SIZE = 1000
FILE_PATH = 'test\\file\\mock'


@pytest.fixture
def umapProjector(mocker):
    """Returns a clean UMAPProjector instance."""
    return UMAPProjector()


class TestUMAPProjector:
    def test_init(self, mocker, umapProjector):
        assert umapProjector.batchSize == UMAP_BATCH_SIZE
        assert umapProjector.overlap == UMAP_OVERLAP

    def test_clearVariables(self, mocker, umapProjector):
        umapProjector.umapModel = "Dummy"
        umapProjector.clearVariables()
        assert umapProjector.umapModel is None
        assert umapProjector.sharedPoints + \
            umapProjector.newPointsPerBatch == umapProjector.batchSize

        umapProjector.batchSize = 3
        umapProjector.overlap = 0.33
        umapProjector.clearVariables()
        assert umapProjector.umapModel is None
        assert umapProjector.sharedPoints == 1
        assert umapProjector.sharedPoints + \
            umapProjector.newPointsPerBatch == umapProjector.batchSize

    def test_clearPointBufferExceptShared(self, umapProjector):
        umapProjector.batchSize = 5
        umapProjector.sharedPoints = 3
        umapProjector.newPointsPerBatch = 2
        umapProjector.bufferFileSize = 10
        bufferResult, countResult = umapProjector.clearPointBufferExceptShared([
            1, 2, 3, 4, 5])

        assert bufferResult == [3, 4, 5]
        assert countResult == 3
        assert umapProjector.bufferFileSize == 0
        assert umapProjector.sizeSoFar == 10

    def test_getPrecision(self, mocker, umapProjector):
        assert umapProjector.getPrecision() == "No supported UMAP precision"

    def test_formatAndCopyModel(self, mocker, umapProjector):
        assert umapProjector.formatAndCopyModel() is None

        umapProjector.umapModel = Mock(spec=UMAP)
        assert isinstance(umapProjector.formatAndCopyModel(), UMAP)

        modelMock = Mock(spec=AlignedUMAP)
        modelMock.mappers_ = ["mappers1", "mappers2", "mappers3"]
        umapProjector.umapModel = modelMock
        model = umapProjector.formatAndCopyModel()
        assert isinstance(model, list)
        assert model == modelMock.mappers_

    def test_start(self, mocker, umapProjector):
        mockGetSize = mocker.patch('os.path.getsize', return_value=FILE_SIZE)
        mockTrain = mocker.patch(SCRIPT_IMPORT_PATH + '.UMAPProjector.train')
        umapProjector.umapModel = "Dummy"

        umapProjector.start(FILE_PATH)

        assert umapProjector.umapModel is None
        assert umapProjector.filePath == FILE_PATH
        assert umapProjector.fileSize == FILE_SIZE
        mockGetSize.assert_called_once()
        assert umapProjector.running
        mockTrain.train.assert_called_once

    def test_train_with_start(self, mocker, umapProjector):
        mocker.patch('sys.getsizeof', return_value=1)
        mocker.patch('os.path.getsize', return_value=6)
        umapMock = Mock()
        umapMockConstruct = mocker.patch(SCRIPT_IMPORT_PATH + '.UMAP', return_value=umapMock)
        alignedMock = MagicMock()
        alignedMockConstruct = mocker.patch(SCRIPT_IMPORT_PATH + '.AlignedUMAP', return_value=alignedMock)

        filedata = 'firstLine 1.0 2.0 3.0\nsecondLine 4.0 5.0 6.0\n' + \
            'thirdLine 1.0 2.0 3.0\nfourthLine 4.0 5.0 6.0\n' + \
            'fifthLine 1.0 2.0 3.0\nsixthLine 4.0 5.0 6.0\n'

        open_mock = mock_open(read_data=filedata)
        with patch('builtins.open', open_mock) as mock_file:
            assert open("path\\doensn't\\matter").read() == filedata

            # Test without running=True
            umapProjector.train()
            assert umapProjector.umapModel is None
            assert not umapProjector.running

            # Test single batch
            umapProjector.batchSize = 10000
            umapProjector.overlap = 0.33
            umapProjector.start(FILE_PATH)
            assert umapProjector.sizeSoFar == 6
            assert not umapProjector.running
            umapMockConstruct.assert_called_once()
            umapMock.fit.assert_called_once()
            umapMock.fit().update.assert_not_called()
            alignedMockConstruct.assert_not_called()
            alignedMock.fit.assert_not_called()

            # Test multiple batches
            umapProjector.batchSize = 3
            umapProjector.overlap = 0.33
            umapProjector.start(FILE_PATH)
            assert umapProjector.sizeSoFar == 6
            assert not umapProjector.running
            alignedMockConstruct.assert_called_once()
            alignedMock.fit.assert_called_once()
            alignedMock.fit().update.assert_called_once()
            umapMockConstruct.assert_called_once()
