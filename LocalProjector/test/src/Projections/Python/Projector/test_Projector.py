import pytest
from src.Projections.Python.Projector.Projector import *


@pytest.fixture
def base_projector():
    """Returns a Projector instance"""
    return Projector()


class TestProjector:
    def test_projector_start(self, base_projector):
        assert not base_projector.running
        assert base_projector.progress == 0

    def test_projector_stop(self, base_projector):
        base_projector.stop()
        assert not base_projector.running

    def test_interface(self, base_projector):
        with pytest.raises(NotImplementedError):
            base_projector.run()
        with pytest.raises(NotImplementedError):
            base_projector.getModel()
        with pytest.raises(NotImplementedError):
            base_projector.train()
