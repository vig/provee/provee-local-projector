import pytest
from unittest.mock import call, Mock, patch, MagicMock, mock_open
from src.Projections.Python.Transformer.ProjectionTransformer import *
import src.generated.Python.transformer_pb2 as protosTransformer


SCRIPT_IMPORT_PATH = 'src.Projections.Python.Transformer.ProjectionTransformer'
FILE_SIZE = 1000
FILE_PATH = 'test\\file\\mock'


@pytest.fixture
def baseProjectionTransformer(mocker):
    """Returns a ProjectionTransformer instance with paused timer."""
    return ProjectionTransformer()


class TestProjectionTransformer:
    def test_init(self, mocker, baseProjectionTransformer):
        assert baseProjectionTransformer.sizeSoFar is None

    def test_clearVariables(self, mocker, baseProjectionTransformer):
        baseProjectionTransformer.sizeSoFar = 10
        baseProjectionTransformer.clearVariables()
        assert baseProjectionTransformer.sizeSoFar is None

    def test_checkIfAbort(self, baseProjectionTransformer):
        # Test running
        baseProjectionTransformer.running = False
        assert baseProjectionTransformer.checkIfAbort(200)
        baseProjectionTransformer.running = True
        assert not baseProjectionTransformer.checkIfAbort(100)

        # Test maxCount
        baseProjectionTransformer.maxCount = 10
        assert baseProjectionTransformer.checkIfAbort(100)

    def test_updatePercentage(self, mocker, baseProjectionTransformer):
        # Test default
        baseProjectionTransformer.updatePercentage()
        assert baseProjectionTransformer.progress == 0
        # Test with no fileSize
        baseProjectionTransformer.sizeSoFar = 10
        baseProjectionTransformer.updatePercentage()
        assert baseProjectionTransformer.progress == 0
        # Test with fileSize
        baseProjectionTransformer.fileSize = FILE_SIZE
        baseProjectionTransformer.updatePercentage()
        assert baseProjectionTransformer.progress == (10 / FILE_SIZE * 100)
        # Test bigger than fileSize
        baseProjectionTransformer.sizeSoFar = FILE_SIZE * 2
        baseProjectionTransformer.updatePercentage()
        assert baseProjectionTransformer.progress == 100

    def test_getProgress(self, mocker, baseProjectionTransformer):
        baseProjectionTransformer.sizeSoFar = 10
        baseProjectionTransformer.fileSize = FILE_SIZE
        progress = int((10 / FILE_SIZE * 100))
        outputAsStr = 'progress: ' + str(progress) + '\n'

        result = baseProjectionTransformer.getProgress(None, None)

        assert str(result) == outputAsStr
        assert isinstance(result, protosTransformer.progressTransformer)

    def test_transformAndToGRPC(self, mocker, baseProjectionTransformer):
        points = [["1", [11, 12, 13]],
                  ["2", [21, 22, 23]],
                  ["3", [31, 32, 33]]]
        outputPoints = [[11, 12],
                        [21, 22],
                        [31, 32]]

        # Mock model.transform
        modelMock = Mock()
        modelMock.transform = Mock(return_value=outputPoints)
        baseProjectionTransformer.model = modelMock

        # Test without clusterIDs
        expected = []
        for i, point in enumerate(outputPoints):
            expected.append(protosTransformer.point(
                id=points[i][0], x=point[0], y=point[1], clusterID=0))
        expected = protosTransformer.pointChunk(points=expected)

        result = baseProjectionTransformer.transformAndToGRPC(points)

        modelMock.transform.assert_called_once_with([point[1] for point in points])
        print('Result', result)
        assert result == expected

        # Test with clusterIDs
        baseProjectionTransformer.isClusterFile = True
        expected = []
        for i, point in enumerate(outputPoints):
            expected.append(protosTransformer.point(
                id=points[i][0], x=point[0], y=point[1], clusterID=int(points[i][0])))
        expected = protosTransformer.pointChunk(points=expected)

        result = baseProjectionTransformer.transformAndToGRPC(points)

        modelMock.transform.assert_called_with([point[1] for point in points])
        print('Result', result)
        assert result == expected

    def test_startTransformerService(self, mocker, baseProjectionTransformer):
        # # Patch size functions
        # mocker.patch(SCRIPT_IMPORT_PATH + '.os.path.getsize', return_value=2)
        # mocker.patch(SCRIPT_IMPORT_PATH + '.sys.getsizeof', return_value=1)
        # # Mock model
        # modelMock = Mock()
        # modelMock.transform = Mock()
        # mocker.patch(SCRIPT_IMPORT_PATH + '.pk.loads', return_value=modelMock)
        # # Mock transform
        # transformAndToGRPCMock = mocker.patch(
        #     SCRIPT_IMPORT_PATH + '.ProjectionTransformer.transformAndToGRPC')
        # # Mock request
        # requestMock = Mock()
        # requestMock.path = Mock(return_value=FILE_PATH)
        # requestMock.model = None

        # filedata = 'firstLine 1.0 2.0 3.0\nsecondLine 4.0 5.0 6.0\n'
        # open_mock = mock_open(read_data=filedata)
        # with patch('builtins.open', open_mock) as mock_file:
        #     assert open("path\\doensn't\\matter").read() == filedata
        #     # Test when running
        #     baseProjectionTransformer.running = True
        #     baseProjectionTransformer.startTransformerService(None, None)
        #     requestMock.path.assert_called_once_with()

        #     # Check init vars of function
        #     assert baseProjectionTransformer.filePath == FILE_PATH
        #     assert baseProjectionTransformer.fileSize == 2

        #     transformAndToGRPCMock.assert_called_once_with(
        #         [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
        # TODO
        return

    def test_printMessage(self, mocker, baseProjectionTransformer):
        # TODO
        return

    def test_serveServer(self, mocker, baseProjectionTransformer):
        # TODO
        return
