import pytest
from unittest.mock import call, Mock, patch, MagicMock, mock_open
from src.constants import *
from src.Projections.Python.Transformer.UMAPTransformer import *
import src.generated.Python.transformer_pb2 as protosTransformer
import numpy as np


SCRIPT_IMPORT_PATH = 'src.Projections.Python.Transformer.UMAPTransformer'
FILE_SIZE = 1000
FILE_PATH = 'test\\file\\mock'


@pytest.fixture
def umapTransformer(mocker):
    """Returns a clean UMAPTransformer instance."""
    return UMAPTransformer()


class TestUMAPTransformer:
    def test_init(self, mocker, umapTransformer):
        assert umapTransformer.batchSize == UMAP_BATCH_SIZE
        assert umapTransformer.overlap == UMAP_OVERLAP
        assert umapTransformer.progress == 0

    def test_clearVariables(self, mocker, umapTransformer):
        umapTransformer.sizeSoFar = 10
        umapTransformer.clearVariables()
        assert umapTransformer.sizeSoFar is None

        umapTransformer.clearVariables()
        assert umapTransformer.bufferSize == UMAP_BATCH_SIZE
        assert umapTransformer.sharedPoints + \
            umapTransformer.newPointsPerBatch == umapTransformer.batchSize

        umapTransformer.batchSize = 3
        umapTransformer.overlap = 0.33
        umapTransformer.clearVariables()
        assert umapTransformer.bufferSize == UMAP_BATCH_SIZE
        assert umapTransformer.sharedPoints == 1
        assert umapTransformer.sharedPoints + \
            umapTransformer.newPointsPerBatch == umapTransformer.batchSize

    def test_alignWithOrigin(self, mocker, umapTransformer):
        def getShared(x):
            return x[:2, :]
        transformed = np.array([[1.0, 1.0], [2.0, 2.0], [4.0, 4.0]])
        result = np.array([[-0.5, -0.5], [0.5, 0.5], [2.5, 2.5]])
        assert (umapTransformer.alignWithOrigin(
            transformed, getShared) == result).all()

        def getEmpty(x):
            return []
        transformed = np.array([[1.0, 1.0], [2.0, 2.0], [4.0, 4.0]])
        result = np.array([[1.0, 1.0], [2.0, 2.0], [4.0, 4.0]])
        assert (umapTransformer.alignWithOrigin(
            transformed, getEmpty) == result).all()

    def test_alignTransformedSets(self, mocker, umapTransformer):
        transformed = np.array([[1.0, 1.0], [2.0, 2.0], [4.0, 4.0]])
        prevShared = np.array([[0.1, 0.1]])
        umapTransformer.batchSize = 3
        umapTransformer.newPointsPerBatch = 2
        umapTransformer.sharedPoints = 1
        umapTransformer.scaling = 1

        aligned = np.array([[0.0, 0.0], [2.0, 2.0], [4.0, 4.0]])
        alignMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.UMAPTransformer.alignWithOrigin', return_value=aligned)

        # First batch
        result, nextShared = umapTransformer.alignTransformedSets(
            transformed, None)
        assert (result == aligned).all()
        assert (nextShared == [aligned[-1]]).all()

        # Later batch
        expected = np.array([[0.0, 0.0], [2.0, 2.0], [4.0, 4.0]])
        result, nextShared = umapTransformer.alignTransformedSets(
            transformed, prevShared)
        assert (result == expected).all()
        assert (nextShared == expected[-1]).all()

        # With scaling
        umapTransformer.scaling = 2
        expected = aligned * 2
        result, nextShared = umapTransformer.alignTransformedSets(
            transformed, None)
        assert (result == expected).all()
        assert (nextShared == expected[-1]).all()

    def test_transformAndToGRPC(self, mocker, umapTransformer):
        def transform(x):
            return x[:, :2]

        def align(x, y):
            return x, x[-1:]

        alignMock = mocker.patch(
            SCRIPT_IMPORT_PATH + '.UMAPTransformer.alignTransformedSets', side_effect=align)

        points = [["1", [11, 12, 13]],
                  ["2", [21, 22, 23]],
                  ["3", [31, 32, 33]],
                  ["4", [41, 42, 43]],
                  ["5", [51, 52, 53]],
                  ["6", [61, 62, 63]]]
        # batch1, batch2, batch3 and an extra batch that should not fit
        batches = [points[:3], points[3:5], points[5:],
                   [["0", [71, 72, 73]]]]

        umapTransformer.batchSize = 3
        umapTransformer.overlap = 0.33
        umapTransformer.clearVariables()

        expected = []
        for batch in batches:
            transformedPointList = []
            for point in batch:
                transformedPointList.append(protosTransformer.point(
                    id=point[0], x=point[1][0], y=point[1][1], clusterID=0))
            expected.append(protosTransformer.pointChunk(
                points=transformedPointList))

        expectedWithClusters = []
        for batch in batches:
            transformedPointList = []
            for point in batch:
                transformedPointList.append(protosTransformer.point(
                    id=point[0], x=point[1][0], y=point[1][1], clusterID=int(point[0])))
            expectedWithClusters.append(protosTransformer.pointChunk(
                points=transformedPointList))

        # Single model
        # no clusterIDs
        umapTransformer.isClusterFile = False
        modelMock = Mock(spec='UMAP')
        modelMock.transform = Mock(side_effect=transform)
        umapTransformer.model = modelMock

        for i, batch in enumerate(batches):
            result = umapTransformer.transformAndToGRPC(batch)
            assert result == expected[i]
            # Assert transform call, convert to string to prevent numpy comparison
            assert str(
                modelMock.transform.mock_calls[-1]) == str(call(np.array([el[1] for el in batch])))
            assert len(modelMock.transform.mock_calls) == i + 1
            alignMock.assert_not_called()

        # with clusterIDs
        umapTransformer.isClusterFile = True

        for i, batch in enumerate(batches):
            result = umapTransformer.transformAndToGRPC(batch)
            assert result == expectedWithClusters[i]
            # Assert transform call, convert to string to prevent numpy comparison
            assert str(
                modelMock.transform.mock_calls[-1]) == str(call(np.array([el[1] for el in batch])))
            alignMock.assert_not_called()

        # Multiple models
        # no clusterIDs
        umapTransformer.isClusterFile = False
        umapTransformer.clearVariables()

        modelMock = [Mock(), Mock(), Mock()]
        for mapper in modelMock:
            mapper.transform = Mock(side_effect=transform)
        umapTransformer.model = modelMock

        # Set clusterID to modelIndex
        expected = []
        for i, batch in enumerate(batches):
            transformedPointList = []
            for point in batch:
                transformedPointList.append(protosTransformer.point(
                    id=point[0], x=point[1][0], y=point[1][1], clusterID=i))
            expected.append(protosTransformer.pointChunk(
                points=transformedPointList))
        expected[-1] = None

        for i, batch in enumerate(batches):
            result = umapTransformer.transformAndToGRPC(batch)
            assert result == expected[i]
            if i < len(modelMock):
                # Assert transform call, convert to string to prevent numpy comparison
                assert str(modelMock[i].transform.mock_calls[-1]
                           ) == str(call(np.array([el[1] for el in (batch if i == 0 else batches[i - 1][-1:] + batch)])))
                assert len(modelMock[i].transform.mock_calls) == 1
                # Assert align call, convert to string to prevent numpy comparison
                assert str(alignMock.mock_calls[-1]) == str(call(np.array([el[1][:2] for el in (batch if i == 0 else batches[i - 1][-1:] + batch)]),
                                                                 None if i == 0 else (np.array([el[1][:2] for el in batches[i - 1][-1:]]))))
                assert len(alignMock.mock_calls) == i + 1

        # with clusterIDs
        umapTransformer.clearVariables()
        umapTransformer.isClusterFile = True
        umapTransformer.model = modelMock

        expectedWithClusters[-1] = None

        for i, batch in enumerate(batches):
            result = umapTransformer.transformAndToGRPC(batch)
            assert result == expectedWithClusters[i]
            if i < len(modelMock):
                # Assert transform call, convert to string to prevent numpy comparison
                assert str(modelMock[i].transform.mock_calls[-1]
                           ) == str(call(np.array([el[1] for el in (batch if i == 0 else batches[i - 1][-1:] + batch)])))
                # Assert align call, convert to string to prevent numpy comparison
                assert str(alignMock.mock_calls[-1]) == str(call(np.array([el[1][:2] for el in (batch if i == 0 else batches[i - 1][-1:] + batch)]),
                                                                 None if i == 0 else (np.array([el[1][:2] for el in batches[i - 1][-1:]]))))
