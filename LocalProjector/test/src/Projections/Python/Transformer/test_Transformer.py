import pytest
from src.Projections.Python.Transformer.Transformer import *


@pytest.fixture
def base_transformer():
    """Returns a Transformer instance."""
    return Transformer()


class TestTransformer:
    def test_transformer_start(self, base_transformer):
        assert base_transformer.running
        assert base_transformer.progress == 0

    def test_transformer_stop(self, base_transformer):
        base_transformer.stop()
        assert not base_transformer.running

    def test_interface(self, base_transformer):
        with pytest.raises(NotImplementedError):
            base_transformer.run()
        with pytest.raises(NotImplementedError):
            base_transformer.transform()
