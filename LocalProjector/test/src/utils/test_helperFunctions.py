import pytest
from src.utils.helperFunctions import lineToVector, distance


class TestHelperFunctions:
    def test_lineToVector(self):
        input = 'testword 1.0  1.2 3.4  5.2'
        output = ('testword', [1.0, 1.2, 3.4, 5.2])
        assert lineToVector(input) == output

    def test_distance(self):
        p0 = (0.0, 0.0)
        p1 = (3, 4)

        assert distance(p0, p1) == pytest.approx(5.0, 0.1)
