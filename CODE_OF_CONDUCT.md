# Code of Conducts

The goal is to maintain a diverse community that's pleasant for everyone. That's why we would greatly appreciate it if everyone contributing to and interacting with the community also followed this Code of Conduct.

The Code of Conduct covers our behavior as members of the community, in any forum, mailing list, wiki, website, Internet relay chat (IRC), public meeting or private correspondence.

_Our Code of Conduct is heavily based on the [Celery Code of Conduct](https://github.com/celery/celery/blob/master/CONTRIBUTING.rst#community-code-of-conduct)_.

## Be considerate
Your work will be used by other people, and you in turn will depend on the work of others. Any decision you take will affect users and colleagues, and we expect you to take those consequences into account when making decisions. Even if it's not obvious at the time, our contributions to PROVEE will impact the work of others. For example, changes to code, infrastructure, policy, documentation and translations during a release may negatively impact others' work.

## Be respectful
The PROVEE community and its members treat one another with respect. Everyone can make a valuable contribution to PROVEE. We may not always agree, but disagreement is no excuse for poor behavior and poor manners. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. It's important to remember that a community where people feel uncomfortable or threatened isn't a productive one. We expect members of the PROVEE community to be respectful when dealing with other contributors as well as with people outside the PROVEE project and with users of PROVEE.

## Be collaborative
Collaboration is central to PROVEE and to the larger free software community. We should always be open to collaboration. Your work should be done transparently and patches from PROVEE should be given back to the community when they're made, not just when the distribution releases. If you wish to work on new code for existing upstream projects, at least keep those projects informed of your ideas and progress. It many not be possible to get consensus from upstream, or even from your colleagues about the correct implementation for an idea, so don't feel obliged to have that agreement before you begin, but at least keep the outside world informed of your work, and publish your work in a way that allows outsiders to test, discuss, and contribute to your efforts.

## When you disagree, consult others
Disagreements, both political and technical, happen all the time and the PROVEE community is no exception. It's important that we resolve disagreements and differing views constructively and with the help of the community and community process. If you really want to go a different way, then we encourage you to make a derivative distribution or alternate set of packages that still build on the work we've done to utilize as common of a core as possible.

## When you're unsure, ask for help
Nobody knows everything, and nobody is expected to be perfect. Asking questions avoids many problems down the road, and so questions are encouraged. Those who are asked questions should be responsive and helpful. However, when asking a question, care must be taken to do so in an appropriate forum.

##  Step down considerately
Developers on every project come and go and PROVEE is no different. When you leave or disengage from the project, in whole or in part, we ask that you do so in a way that minimizes disruption to the project. This means you should tell people you're leaving and take the proper steps to ensure that others can pick up where you left off.
