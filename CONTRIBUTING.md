# Contributing

## Introduction

First off, thank you for considering contributing to Provee! Most likely that means that you came across some limitations of other systems or you 
just want to play around with the coolest newest technology (like we do 😎). 

### BUT why should I read this!??!

Our contribution guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your pull requests.


### Which kind of contributions we are searching for.

PROVEE is an open source project with academic background. We love to receive contributions from our community — you! Generally, all contributions are more than welcome. Ideas for non-conventional contributions can be writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into Provee itself.


<!-- ### Explain contributions you are NOT looking for (if any).

Again, defining this up front means less work for you. If someone ignores your guide and submits something you don’t want, you can simply close it and point to your policy.

> Please, don't use the issue tracker for [support questions]. Check whether the #pocoo IRC channel on Freenode can help with your issue. If your problem is not strictly Werkzeug or Flask specific, #python is generally more active. Stack Overflow is also worth considering.

[source: [Flask](https://github.com/pallets/flask/blob/master/CONTRIBUTING.rst)] **Need more inspiration?** [1] [cucumber-ruby](https://github.com/cucumber/cucumber-ruby/blob/master/CONTRIBUTING.md#about-to-create-a-new-github-issue) [2] [Read the Docs](http://read-the-docs.readthedocs.org/en/latest/open-source-philosophy.html#unsupported) -->

## Code of Conducts

Please find our Code of Conduct here. We that this serious! [Code of Conduct for more fun and happiness in PROVEE](../CODE_OF_CONDUCT.md)


## Your First Contribution

> Unsure where to begin contributing to Atom? You can start by looking through `beginner` and `help-wanted` issues:

```
Beginner issues - issues which should only require a few lines of code, and a test or two.
Help wanted issues - issues which should be a bit more involved than beginner issues.
```

Issue lists can be sorted by total number of comments. While not perfect, number of comments is a reasonable proxy for impact a given change will have.


### Never contributed to open source before? No problemo
Here are a couple of friendly tutorials: http://makeapullrequest.com/ and http://www.firsttimersonly.com/ or use this *free* series, [How to Contribute to an Open Source Project on GitHub](https://egghead.io/series/how-to-contribute-to-an-open-source-project-on-github).

At this point, you're ready to make your changes! Feel free to ask for help; everyone is a beginner at first :smile_cat:
If a maintainer asks you to `rebase` your PR, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge.

## Getting started
For something that is bigger than a one or two line fix:

1. Create your own fork of the code
2. Do the changes in your fork
3. If you like the change and think the project could use it:
    * Be sure you have followed the code style for the project.
    <!-- * Sign the Contributor License Agreement, CLA, with the jQuery Foundation. -->
    * Note the Provee Code of Conduct.
    * Send a pull request.

<!-- [source: [Requirejs](http://requirejs.org/docs/contributing.html)] **Need more inspiration?** [1] [Active Admin](https://github.com/activeadmin/activeadmin/blob/master/CONTRIBUTING.md#1-where-do-i-go-from-here) [2] [Node.js](https://github.com/nodejs/node/blob/master/CONTRIBUTING.md#code-contributions) [3] [Ember.js](https://github.com/emberjs/ember.js/blob/master/CONTRIBUTING.md#pull-requests) -->

Small contributions such as fixing spelling errors, where the content is small enough to not be considered intellectual property, can be submitted by a contributor as a patch.  
As a rule of thumb, changes are obvious fixes if they do not introduce any new functionality or creative thinking. As long as the change does not affect functionality, some likely examples include the following:
* Spelling / grammar fixes
* Typo correction, white space and formatting changes
* Comment clean up
* Bug fixes that change default return values or error codes stored in constants
* Adding logging messages or debugging output
* Changes to ‘metadata’ files like Gemfile, .gitignore, build scripts, etc.
* Moving source files from one directory or package to another


## Code, commit message and labeling conventions
These sections should streamline the contributions you make to PROVEE.

### Preferred style for code.

TDB

### Commit message conventions.

We are using semantic versioning [semver](semver.org) and particularly [semantic-release](https://github.com/semantic-release/semantic-release#readme) in this project. Release Notes and Changelogs are automatically generated. It is important that you obey the [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)

Each commit message consists of a mandatory header and optionally a body and/or a footer. The header has a special format that includes a type, a scope and a subject. Here `<type>(<scope>): <subject>` is the header:
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

#### Header

**Type** must be one of the following:

- feat: A new feature
- fix: A bug fix
- docs: Documentation only changes
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- refactor: A code change that neither fixes a bug nor adds a feature
- perf: A code change that improves performance
- test: Adding missing or correcting existing tests
- ci: Changes to the GitLab Pipeline
- chore: Changes to the build process or auxiliary tools and libraries

**Scope**

The scope is optional. It is used to show which part of the project is affected. Therefore use it for a filename (without extenstion), a folder name to describe the part of the project changed. If a change affects a lot of parts in the project the scope should be left blank, e.g. `test: added missing unittests` affects the whole test folder so the scope is redundant.

**Subject**

Use the subject to give a brief summary of the changes. Do this in the present tense and start with a lower case, e.g. `feat(transformer): change transformer to support new projector types`. Note the usage of 'change' instead of 'changed' or 'changes'.

#### Body

The body can be used to further elaborate on the changes and the motivation behind it. For example:

```
fix(transformer): fix transformer crashing at end of transforming points

The transformer kept requesting points even when finished causing an error. The transformer now first checks if all points have been handled.
```

#### Footer

The footer can be used to name breaking changes. Breacking changes are changes that cause backwards incompatibility. The footer should contain the changes and instructions to roll back if possible. Here is an example without a body:

```
perf(projector): remove projection type option, default set to PCA

BREAKING CHANGE: Support for projections other than PCA have been removed to improve performance.
```

For more information see [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) or for examples see [AngularJS Git Commit Message Conventions](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#heading=h.uyo6cb12dt6w).

### Usage

Your code can be merged to master by making a merge request. Give the merge request the same title as your header. Enable the checkbox for `Squash commits`. Click on `Modify commit messages` and make sure that the **Squash commit message** follows the commit message convention. GitLab should automatically set the message to the merge request title which functions as header, but you can also add a body and footer, both having a new blank line above them. When your merge request is approved by an assignee and merged the pipeline will run semantic release and update the version and changelog according to your squashed commit message.

### Labeling conventions for issues

We try to follow the [StandardIssueLabels](https://github.com/wagenet/StandardIssueLabels#standardissuelabels) for open source projects

## How to report a bug
### Security FIRST!
Any security issues should be submitted directly to m.behrisch at uu.nl  
In order to determine whether you are dealing with a security issue, ask yourself these two questions:
 * Can I access something that's not mine, or something I shouldn't have access to?
 * Can I disable something for other people?

If the answer to either of those two questions are "yes", then you're probably dealing with a security issue. Note that even if you answer "no" to both questions, you may still be dealing with a security issue, so if you're unsure, just email us at m.behrisch at uu.nl.

### How to file a bug report.
When filing an issue, make sure to answer these five questions:

1. What version of PROVEE are you using (VERSION.txt file)?
2. What operating system and browser are you using?
3. What did you do?
4. What did you expect to see?
5. What did you see instead?
General questions should go to the provee mailing list instead of the issue tracker. The provee-community will directly answer or ask you to file an issue if you've tripped over a bug.

## How to suggest a feature or enhancement
If you find yourself wishing for a feature that doesn't exist in PROVEE, you are probably not alone. There are bound to be others out there with similar needs. Many of the features that PROVEE has today have been added because our users saw the need. Open an issue on our issues list on GitLAB which describes the feature you would like to see, why you need it, and how it should work.

## Code review process

The core team looks at Pull Requests on a regular basis in a weekly triage meeting that we hold in a public Teams meeting (link in the [README](../README.md) under contributing).  
After feedback has been given we expect responses within two weeks. After two weeks we may close the pull request if it isn't showing any activity.

<!-- # Community

You can chat with the core team on https://gitter.im/cucumber/cucumber. We try to have office hours on Fridays.

[source: [cucumber-ruby](https://github.com/cucumber/cucumber-ruby/blob/master/CONTRIBUTING.md#talking-with-other-devs)] **Need more inspiration?**
 [1] [Chef](https://github.com/chef/chef/blob/master/CONTRIBUTING.md#-developer-office-hours) [2] [Cookiecutter](https://github.com/audreyr/cookiecutter#community)
 -->
