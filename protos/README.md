# provee-protos

Contains all gRPC proto files that are used to communicate between services.
Execute build.sh to generate the protos files. They will automatically be placed at the right location. You can also use the command mentioned below. 

## Dependencies

```
pip install grpcio-tools
```

## Development

Generate gRPC service classes whenever protos change.

```
./build.sh
```

If you have problems with running build.sh you can try running the following command from provee/protos instead:

```
python -m grpc_tools.protoc -I "./" --python_out="../LocalProjector" --grpc_python_out="../LocalProjector" ./src/generated/Python/*.proto
```
