#!/bin/bash
DIR="$(dirname "$0")"
PROTOS_DIRECTORY="$PWD"
PYTHON_PROTO_FILES="$PROTOS_DIRECTORY/src/generated/Python/*.proto"

generate_python_files()
{       
        proto_file=$1
        python -m grpc_tools.protoc \
        -I "$PROTOS_DIRECTORY" \
        --python_out="../LocalProjector" \
        --grpc_python_out="../LocalProjector" \
        $proto_file
}

#PROTO files
generate_python_files "$PYTHON_PROTO_FILES"


echo "Done. Closes in 5 sec."
sleep 5